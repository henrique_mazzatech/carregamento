package com.refinaria.carregamento.viewmodel;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.refinaria.carregamento.model.EstruturaPickListModel;
import com.refinaria.carregamento.model.LoginModel;
import com.refinaria.carregamento.model.ProdutosPickListModel;
import com.refinaria.carregamento.model.Rowset;
import com.refinaria.carregamento.services.IPickListService;
import com.refinaria.carregamento.services.IServiceCallback;
import com.refinaria.carregamento.services.SharedPref;
import com.refinaria.carregamento.services.api.ApiModule;
import com.refinaria.carregamento.services.impl.PickListService;
import com.refinaria.carregamento.utils.Constantes;
import com.refinaria.carregamento.utils.DataUtils;
import com.refinaria.carregamento.utils.PickListUtils;
import com.refinaria.carregamento.utils.ScanUtils;
import com.refinaria.carregamento.utils.SingleLiveEvent;
import com.refinaria.carregamento.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class ProdutoViewModel extends ViewModel {

    private MutableLiveData<String> remessa = new MutableLiveData<>();
    private MutableLiveData<String> numeroPedido = new MutableLiveData<>();
    private MutableLiveData<Boolean> tipoFaturamento = new MutableLiveData<>();
    private MutableLiveData<String> numeroPallet = new MutableLiveData<>();
    private MutableLiveData<String> quantidadeFracionada = new MutableLiveData<>();
    private MutableLiveData<ProdutosPickListModel> pickListMutableLiveData = new MutableLiveData<>();

    private ProdutosPickListModel produtosPickListModel;

    private IPickListService service = new PickListService();

    private MutableLiveData<Boolean> checkedQuantidadeFracionada = new MutableLiveData<>();
    private MutableLiveData<Boolean> camera = new MutableLiveData<>();
    private MutableLiveData<String> errorMessage = new MutableLiveData<>();
    private MutableLiveData<String> retornoMessage = new MutableLiveData<>();
    private MutableLiveData<List<Rowset>> mutableProdutoList = new MutableLiveData<>();
    private MutableLiveData<Boolean> progressBar = new MutableLiveData<>();
    private SingleLiveEvent<List<Rowset>> conferirProdutos = new SingleLiveEvent<>();

    private List<Rowset> produtoList = new ArrayList<>();
    private List<Rowset> arrayList;

    public ProdutoViewModel(ProdutosPickListModel produtosPickListModel) {
        this.produtosPickListModel = produtosPickListModel;
        hideProgressBar();
        init();
    }

    public void init() {
        hideProgressBar();

        checkedQuantidadeFracionada.setValue(false);
        remessa.setValue(SharedPref.read(Constantes.FLAG_REMESSA, ""));

        numeroPedido.setValue(SharedPref.read(Constantes.FLAG_PEDIDO, ""));
        tipoFaturamento.setValue(SharedPref.read(Constantes.FLAG_FATURAMENTO, false));

        if (arrayList == null)
            arrayList = Utils.filtraRowsetArray(produtosPickListModel.getServiceRequest1().getFsP5542041W5542041B().getData().getGridData().getRowset());

        preparaLista(arrayList);
    }

    public void recebeQrCode(String qrCode) {
        trataQrCode(qrCode);
        onClickAdd();
    }

    private void trataQrCode(String qrCode) {
        numeroPallet.setValue(qrCode.isEmpty() ? "" : ScanUtils.getNumeroPalete(qrCode));
    }


    private void preparaLista(List<Rowset> arrayList) {
        if (produtosPickListModel == null) return;

        if (arrayList.size() > 0) {
            produtoList = arrayList;
            List<Rowset> newArrayList = new ArrayList<>(produtoList);
            Collections.reverse(newArrayList);

            mutableProdutoList.setValue(produtoList);
        }
    }

    public void onClickAdd() {
        if (checkData()) {
            showProgressBar();
            EstruturaPickListModel estruturaPickListModel = montaPickListAdicao();
            Log.i("estruturaPickListModel", ApiModule.getGsonInstance().toJson(estruturaPickListModel));

            service.carregarPickList(LoginModel.getToken(), estruturaPickListModel, new IServiceCallback<ProdutosPickListModel>() {
                @Override
                public void onSuccessResponse(Call<ProdutosPickListModel> call, Response<ProdutosPickListModel> response) {
                    Log.i("onSuccessResponseAdd", ApiModule.getGsonInstance().toJson(response));

                    trataRetornoAdicionaProduto(response);
                    hideProgressBar();
                }

                @Override
                public void onErrorResponse(Call<ProdutosPickListModel> call, Throwable throwable) {
                    throwable.printStackTrace();
                    retornoMessage.setValue(throwable.getLocalizedMessage());
                    hideProgressBar();
                }
            });
        }
    }

    public void onClickConferir() {
        Log.i("onClickRepaletiza", ApiModule.getGsonInstance().toJson(arrayList));
        conferirProdutos.setValue(arrayList);
    }

    private void trataRetornoAdicionaProduto(Response<ProdutosPickListModel> response) {
        pickListMutableLiveData.setValue(response.body());
        if (pickListMutableLiveData.getValue() != null) {
            if (response.body() != null) {
                String message = response.body().getServiceRequest1().getFsP5542041W5542041B().getData().getGridData().getRowset().get(0).getZD20060();
                if (message.equals(" ")) {
                    message = Constantes.TOAST_PRODUTO_ADICIONADO;
                    produtoList.add(0, response.body().getServiceRequest1().getFsP5542041W5542041B().getData().getGridData().getRowset().get(0));
                    arrayList = produtoList;
                    mutableProdutoList.setValue(produtoList);
                }
                retornoMessage.setValue(message);
            }
        }

        numeroPallet.setValue("");
        quantidadeFracionada.setValue("");
        checkedQuantidadeFracionada.setValue(false);
        SharedPref.remove(Constantes.FLAG_QR_CODE);
    }

    @SuppressWarnings("ConstantConditions")
    private EstruturaPickListModel montaPickListAdicao() {
        if (checkedQuantidadeFracionada.getValue())
            return montaEstruturaProdutoFracionado();
        else
            return montaEstruturaPickList();
    }

    public void onCancelProduto(int position) {
        showProgressBar();
        EstruturaPickListModel estruturaPickListModel = montaEstruturaCancelaProduto(position);
        Log.i("estruturaPickListModel", ApiModule.getGsonInstance().toJson(estruturaPickListModel));
        Log.i("produtoList", ApiModule.getGsonInstance().toJson(produtoList));

        service.carregarPickList(LoginModel.getToken(), estruturaPickListModel, new IServiceCallback<ProdutosPickListModel>() {
            @Override
            public void onSuccessResponse(Call<ProdutosPickListModel> call, Response<ProdutosPickListModel> response) {
                Log.i("response", ApiModule.getGsonInstance().toJson(response));

                trataRetornoCancelaProduto(position, response);
                hideProgressBar();
            }

            @Override
            public void onErrorResponse(Call<ProdutosPickListModel> call, Throwable throwable) {
                throwable.printStackTrace();
                retornoMessage.setValue(throwable.getLocalizedMessage());
                hideProgressBar();
            }
        });
    }

    private void trataRetornoCancelaProduto(int position, Response<ProdutosPickListModel> response) {
        if (response.code() == 200 && response.body() != null) {
            String message = response.body().getServiceRequest1().getFsP5542041W5542041B().getData().getGridData().getRowset().get(0).getZD20060();
            if (message.equals(MensagensCancelamento.SUCESSO.getValue())) {
                produtoList.remove(position);
                mutableProdutoList.setValue(produtoList);
                retornoMessage.setValue(message);
            } else {
                retornoMessage.setValue(message);
            }
        } else {
            retornoMessage.setValue(MensagensCancelamento.FALHA.getValue());
        }
    }

    private EstruturaPickListModel montaEstruturaPickList() {
        return PickListUtils.montaEstruturaPickList(remessa.getValue(), numeroPedido.getValue(), numeroPallet.getValue());
    }

    private EstruturaPickListModel montaEstruturaProdutoFracionado() {
        return PickListUtils.montaEstruturaPickListFracionada(
                remessa.getValue(),
                numeroPedido.getValue(),
                numeroPallet.getValue(),
                quantidadeFracionada.getValue());
    }

    private EstruturaPickListModel montaEstruturaCancelaProduto(int position) {
        return PickListUtils.montaEstruturaCancelaProduto(
                String.valueOf(produtoList.get(position).getZSHPN70()),
                String.valueOf(produtoList.get(position).getZOTORDRLS67()),
                String.valueOf(produtoList.get(position).getZCNID59()),
                String.valueOf(produtoList.get(position).getZQNTA68()));
    }

    public void bindingValidacaoNumeroPallet() {
        errorMessage.setValue(ScanUtils.validaNumeroPallet(numeroPallet.getValue()));
    }

    private boolean checkData() {
        if (!DataUtils.isCampoValido(remessa.getValue(), 5))
            errorMessage.setValue(Constantes.ERRO_TAMANHO_REMESSA_INVALIDO);
        else if (!DataUtils.isCampoValido(numeroPedido.getValue(), 14))
            errorMessage.setValue(Constantes.ERRO_NUMERO_PEDIDO_INVALIDO);
        else if (!DataUtils.isCampoValido(numeroPallet.getValue(), 5))
            errorMessage.setValue((Constantes.ERRO_TAMANHO_NUMERO_PALLET_INVALIDO));
        else {
            errorMessage.setValue("");
            return true;
        }
        return false;
    }

    private void showProgressBar() {
        progressBar.setValue(true);
    }

    private void hideProgressBar() {
        progressBar.setValue(false);
    }

    public MutableLiveData<List<Rowset>> getMutableProdutoList() {
        return mutableProdutoList;
    }

    public MutableLiveData<Boolean> getCheckedQuantidadeFracionada() {
        return checkedQuantidadeFracionada;
    }

    public MutableLiveData<String> getNumeroPedido() {
        return numeroPedido;
    }

    public MutableLiveData<Boolean> getCamera() {
        return camera;
    }

    public MutableLiveData<String> getRemessa() {
        return remessa;
    }

    public MutableLiveData<String> getErrorMessage() {
        return errorMessage;
    }

    public MutableLiveData<String> getRetornoMessage() {
        return retornoMessage;
    }

    public MutableLiveData<String> getNumeroPallet() {
        return numeroPallet;
    }

    public MutableLiveData<String> getQuantidadeFracionada() {
        return quantidadeFracionada;
    }

    public MutableLiveData<Boolean> getProgressBar() {
        return progressBar;
    }

    public SingleLiveEvent<List<Rowset>> getConferirProdutos() {
        return conferirProdutos;
    }

    public MutableLiveData<Boolean> getTipoFaturamento() {
        return tipoFaturamento;
    }
}

enum MensagensCancelamento {

    SUCESSO("Pallet Cancelado"),
    FALHA("Erro ao cancelar o pallet");

    private final String value;

    MensagensCancelamento(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}