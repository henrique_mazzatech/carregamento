package com.refinaria.carregamento.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.refinaria.carregamento.model.Saldo;
import com.refinaria.carregamento.services.impl.ConsultaSaldoService;
import com.refinaria.carregamento.utils.ConsultaSaldoUtils;
import com.refinaria.carregamento.utils.ScanUtils;

import retrofit2.Response;

public class ConsultaSaldoViewModel extends ViewModel {

    private MutableLiveData<String> errorMessage = new MutableLiveData<>();
    private MutableLiveData<Saldo> saldo = new MutableLiveData<>();
    private MutableLiveData<Boolean> progressBar = new MutableLiveData<>();
    private MutableLiveData<Boolean> cardViewVisible = new MutableLiveData<>();
    private MutableLiveData<String> numeroPallet = new MutableLiveData<>();

    private ConsultaSaldoService service = new ConsultaSaldoService(this);

    public void consultaSaldo() {
        showProgressBar();
        cardViewVisible.setValue(false);
        service.getSaldo(ConsultaSaldoUtils.montaEstruturaConsultaSaldo(numeroPallet.getValue()));
    }

    public void getSaldoOnSuccess(Response<Saldo> response) {
        hideProgressBar();

        numeroPallet.setValue("");
        if (response.body() != null) {
            saldo.setValue(response.body());
            cardViewVisible.setValue(true);
        }
    }

    public void recebeQrCode(String qrCode) {
        trataQrCode(qrCode);
        consultaSaldo();
    }

    private void trataQrCode(String qrCode) {
        numeroPallet.setValue(qrCode.isEmpty() ? "" : ScanUtils.getNumeroPalete(qrCode));
    }

    public void validarNumeroPallet() {
        errorMessage.setValue(ScanUtils.validaNumeroPallet(numeroPallet.getValue()));
    }

    private void showProgressBar() {
        progressBar.setValue(true);
    }

    private void hideProgressBar() {
        progressBar.setValue(false);
    }

    public MutableLiveData<String> getNumeroPallet() {
        return numeroPallet;
    }

    public void setNumeroPallet(MutableLiveData<String> numeroPallet) {
        this.numeroPallet = numeroPallet;
    }

    public MutableLiveData<Saldo> getSaldo() {
        return saldo;
    }

    public MutableLiveData<Boolean> getProgressBar() {
        return progressBar;
    }

    public MutableLiveData<Boolean> getCardViewVisible() {
        return cardViewVisible;
    }
}