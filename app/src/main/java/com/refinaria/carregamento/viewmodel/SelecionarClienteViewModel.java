package com.refinaria.carregamento.viewmodel;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.refinaria.carregamento.model.EstruturaPickListModel;
import com.refinaria.carregamento.model.LoginModel;
import com.refinaria.carregamento.model.RepaletizacaoResponse;
import com.refinaria.carregamento.model.clientes.ListaClientes;
import com.refinaria.carregamento.model.clientes.Rowset;
import com.refinaria.carregamento.services.IPickListService;
import com.refinaria.carregamento.services.IServiceCallback;
import com.refinaria.carregamento.services.SharedPref;
import com.refinaria.carregamento.services.impl.PickListService;
import com.refinaria.carregamento.utils.RepaletizacaoUtils;

import java.util.ArrayList;
import java.util.Collections;

import retrofit2.Call;
import retrofit2.Response;

import static com.refinaria.carregamento.utils.Constantes.SHARED_COD_CLIENTE;
import static com.refinaria.carregamento.utils.Constantes.SHARED_COD_PROD;
import static com.refinaria.carregamento.utils.Constantes.SHARED_DOCUMENTO;
import static com.refinaria.carregamento.utils.Constantes.TOAST_SELECIONE_UM_CLIENTE;

public class SelecionarClienteViewModel extends ViewModel {

    private IPickListService service = new PickListService();

    private MutableLiveData<Boolean> progressBar = new MutableLiveData<>();
    private MutableLiveData<String> returnMessage = new MutableLiveData<>();
    private MutableLiveData<ArrayList<Rowset>> listaClientes = new MutableLiveData<>();
    private MutableLiveData<Boolean> sucessoRepaletizacao  = new MutableLiveData<>();

    private String documento;
    private String codigoProduto;
    private String qtdCliente;

    public void init() {
        SharedPref.remove(SHARED_COD_CLIENTE);

        codigoProduto = SharedPref.read(SHARED_COD_PROD, "");
        documento = SharedPref.read(SHARED_DOCUMENTO, "");

        listaCliente();
    }

    private void listaCliente() {
        showProgressBar();
        EstruturaPickListModel estruturaPickListModel = RepaletizacaoUtils.estruturaListaClientes(codigoProduto);
        Log.i("listaCliente", new Gson().toJson(estruturaPickListModel));

        service.listarClientes(estruturaPickListModel, LoginModel.getToken(), new IServiceCallback<ListaClientes>() {
            @Override
            public void onSuccessResponse(Call<ListaClientes> call, Response<ListaClientes> response) {
                Log.i("listaClienteResponseSuc", new Gson().toJson(response));
                if (response.body() != null) {
                    ArrayList<Rowset> list1 = new ArrayList<>();
                    Collections.addAll(list1, response.body().getJDE_SREQ_CONSULTA_PALETIZACAO_ESP_1().getRowset());
                    listaClientes.setValue(list1);
                }
                hideProgressBar();
            }

            @Override
            public void onErrorResponse(Call<ListaClientes> call, Throwable throwable) {
                throwable.printStackTrace();
                returnMessage.setValue(throwable.getLocalizedMessage());
                hideProgressBar();
            }
        });
    }

    public void finalizaRepaletizacao() {
        showProgressBar();

        leDados();

        if(qtdCliente.isEmpty()) {
            returnMessage.setValue(TOAST_SELECIONE_UM_CLIENTE);
            hideProgressBar();
            return;
        }

        EstruturaPickListModel estruturaPickListModel = RepaletizacaoUtils.estruturaFinalizaRepaletizacao(String.valueOf(7013), documento, qtdCliente);

        Log.i("estruturaPickListModel", new Gson().toJson(estruturaPickListModel));

        service.adicionarProdutoRepaletizacao(estruturaPickListModel, LoginModel.getToken(), new IServiceCallback<RepaletizacaoResponse>() {
            @Override
            public void onSuccessResponse(Call<RepaletizacaoResponse> call, Response<RepaletizacaoResponse> response) {
                Log.i("onSuccess", new Gson().toJson(response));

                if (response.body() != null && (response.body().getMensagem() == null || (response.body().getMensagem().isEmpty() || response.body().getMensagem().equals(" ")))) {
                    sucessoRepaletizacao.setValue(true);
                }
                returnMessage.setValue(response.body() != null ? response.body().getMensagem() : null);
                hideProgressBar();
            }

            @Override
            public void onErrorResponse(Call<RepaletizacaoResponse> call, Throwable throwable) {
                throwable.printStackTrace();
                returnMessage.setValue(throwable.getLocalizedMessage());
                hideProgressBar();
            }
        });
    }

    private void leDados() {
        qtdCliente = SharedPref.read(SHARED_COD_CLIENTE, "");
        codigoProduto = SharedPref.read(SHARED_COD_PROD, "");
        documento = SharedPref.read(SHARED_DOCUMENTO, "");
    }

    private void showProgressBar() {
        progressBar.setValue(true);
    }

    private void hideProgressBar() {
        progressBar.setValue(false);
    }

    public MutableLiveData<ArrayList<Rowset>> getListaClientes() {
        return listaClientes;
    }

    public MutableLiveData<String> getReturnMessage() {
        return returnMessage;
    }

    public MutableLiveData<Boolean> getProgressBar() {
        return progressBar;
    }

    public MutableLiveData<Boolean> getSucessoRepaletizacao() {
        return sucessoRepaletizacao;
    }
}