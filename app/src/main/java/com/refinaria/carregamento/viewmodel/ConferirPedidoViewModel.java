package com.refinaria.carregamento.viewmodel;

import android.os.Build;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.refinaria.carregamento.model.EstruturaPickListModel;
import com.refinaria.carregamento.model.LoginModel;
import com.refinaria.carregamento.model.ProdutosPickListModel;
import com.refinaria.carregamento.model.Rowset;
import com.refinaria.carregamento.services.IServiceCallback;
import com.refinaria.carregamento.services.SharedPref;
import com.refinaria.carregamento.services.api.ApiModule;
import com.refinaria.carregamento.services.impl.PickListService;
import com.refinaria.carregamento.utils.Constantes;
import com.refinaria.carregamento.utils.PickListUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

import static com.refinaria.carregamento.utils.Utils.filtraRowsetArray;

public class ConferirPedidoViewModel extends ViewModel {

    private MutableLiveData<List<Rowset>> pedidoList = new MutableLiveData<>();
    private MutableLiveData<List<Rowset>> pedidoListInicial = new MutableLiveData<>();

    private MutableLiveData<String> remessa = new MutableLiveData<>();
    private MutableLiveData<String> numeroPedido = new MutableLiveData<>();
    private MutableLiveData<Boolean> tipoFaturamento = new MutableLiveData<>();
    private MutableLiveData<String> mensagemConfere = new MutableLiveData<>();
    private MutableLiveData<Boolean> progressBar = new MutableLiveData<>();

    private PickListService service = new PickListService();

    public ConferirPedidoViewModel(Rowset[] produtosPickListModel) {
        init(produtosPickListModel);
    }

    private void init(Rowset[] produtosPickListModel) {
        remessa.setValue(SharedPref.read(Constantes.FLAG_REMESSA, ""));
        numeroPedido.setValue(SharedPref.read(Constantes.FLAG_PEDIDO, ""));

        tipoFaturamento.setValue(SharedPref.read(Constantes.FLAG_FATURAMENTO, false));

        trataProdutoPickList(produtosPickListModel);
    }

    private void trataProdutoPickList(Rowset[] produtosPickListModel) {

        List<Rowset> arrayList = filtraRowsetArray(Arrays.asList((produtosPickListModel)));
        List<Rowset> returnArray = new ArrayList<>(cloneList(arrayList));

        returnArray = new ArrayList<>(agrupaPedidos(returnArray));

        pedidoList.setValue(returnArray);
        pedidoListInicial.setValue(arrayList);
    }

    private static List<Rowset> cloneList(List<Rowset> list) {
        List<Rowset> clone = new ArrayList<>(list.size());
        for (Rowset rowset : list) {
            try {
                clone.add((Rowset) rowset.clone());
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
        }
        return clone;
    }

    private List<Rowset> agrupaPedidos(List<Rowset> arrayList) {

        Collections.sort(arrayList, (rowset, t1) -> rowset.getZLITM64().compareToIgnoreCase(t1.getZLITM64()));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            for (int i = 0; i < arrayList.size() - 1; i++) {
                if (arrayList.get(i).getZLITM64().equals(arrayList.get(i + 1).getZLITM64())) {
                    Rowset rowset = arrayList.get(i);
                    rowset.setZQNTA68(arrayList.get(i).getZQNTA68() + arrayList.get(i + 1).getZQNTA68());
                    arrayList.set(i, rowset);
                    arrayList.remove(i + 1);
                    i--;
                }
            }
        }

        return arrayList;
    }

    public void onClickConferir() {
        showProgressBar();
        EstruturaPickListModel estruturaPickListModel = montaPickListConferir();
        Log.i("estruturaPickListModel", ApiModule.getGsonInstance().toJson(estruturaPickListModel));

        service.carregarPickList(LoginModel.getToken(), estruturaPickListModel, new IServiceCallback<ProdutosPickListModel>() {
            @Override
            public void onSuccessResponse(Call<ProdutosPickListModel> call, Response<ProdutosPickListModel> response) {
                ProdutosPickListModel produtosPickListModel = ApiModule.getGsonInstance().fromJson(ApiModule.getGsonInstance().toJson(response.body()), ProdutosPickListModel.class);
                String mensagem = pegaMensagem(produtosPickListModel);

                if (!mensagem.equals("")) {
                    hideProgressBar();
                    mensagemConfere.setValue(mensagem);
                }
            }

            @Override
            public void onErrorResponse(Call<ProdutosPickListModel> call, Throwable t) {
                mensagemConfere.setValue(t.getLocalizedMessage());
                hideProgressBar();
                t.printStackTrace();
            }
        });
    }

    private String pegaMensagem(ProdutosPickListModel produtosPickListModel) {
        String mensagem = "";
        try {
            mensagem = produtosPickListModel.getServiceRequest1().getFsP5542041W5542041B().getData().getGridData().getRowset().get(0).getZD20060();
            if (mensagem.contains("Não existe esse produto no pedido")) {
                mensagem += "\n\nPallet: " + produtosPickListModel.getServiceRequest1().getFsP5542041W5542041B().getData().getGridData().getRowset().get(0).getZCNID59();
                mensagem += "\n\nQuantidade Esperada: " + produtosPickListModel.getServiceRequest1().getFsP5542041W5542041B().getData().getGridData().getRowset().get(0).getZQNTA68();
            } else if (mensagem.contains("Quantidade divergente do total do pedido")) {
                mensagem += "\n\nQuantidade Esperada: " + produtosPickListModel.getServiceRequest1().getFsP5542041W5542041B().getData().getGridData().getRowset().get(0).getZQNTA68();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mensagem;
    }

    private void showProgressBar() {
        progressBar.setValue(true);
    }

    private void hideProgressBar() {
        progressBar.setValue(false);
    }

    private EstruturaPickListModel montaPickListConferir() {
        return PickListUtils.montaEstruturaPickListConferir(remessa.getValue(), numeroPedido.getValue());
    }

    public MutableLiveData<String> getRemessa() {
        return remessa;
    }

    public MutableLiveData<String> getNumeroPedido() {
        return numeroPedido;
    }

    public MutableLiveData<List<Rowset>> getPedidoList() {
        return pedidoList;
    }

    public MutableLiveData<List<Rowset>> getPedidoListInicial() {
        return pedidoListInicial;
    }

    public MutableLiveData<Boolean> getTipoFaturamento() {
        return tipoFaturamento;
    }

    public MutableLiveData<String> getMensagemConfere() {
        return mensagemConfere;
    }

    public void setMensagemConfere(MutableLiveData<String> mensagemConfere) {
        this.mensagemConfere = mensagemConfere;
    }

    public MutableLiveData<Boolean> getProgressBar() {
        return progressBar;
    }

    public void setProgressBar(MutableLiveData<Boolean> progressBar) {
        this.progressBar = progressBar;
    }
}
