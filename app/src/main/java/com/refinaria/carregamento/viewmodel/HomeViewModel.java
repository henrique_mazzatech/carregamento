package com.refinaria.carregamento.viewmodel;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.refinaria.carregamento.model.LoginModel;
import com.refinaria.carregamento.services.IServiceCallback;
import com.refinaria.carregamento.services.auth.Auth;
import com.refinaria.carregamento.services.impl.LoginService;
import com.refinaria.carregamento.services.impl.PickListService;
import com.refinaria.carregamento.utils.Constantes;
import com.refinaria.carregamento.utils.Matricula;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;

import static com.refinaria.carregamento.utils.Constantes.CODIGO_CABO_FRIO;
import static com.refinaria.carregamento.utils.Constantes.CODIGO_CABO_FRIO_MENSALISTA;
import static com.refinaria.carregamento.utils.Constantes.SENHA_WSR;
import static com.refinaria.carregamento.utils.Constantes.USUARIO_WSR;

public class HomeViewModel extends ViewModel {

    private MutableLiveData<Boolean> progressBar = new MutableLiveData<>();
    private MutableLiveData<Boolean> showDialogoMatricula = new MutableLiveData<>();
    private MutableLiveData<Boolean> matriculaValida = new MutableLiveData<>();
    private MutableLiveData<String> message = new MutableLiveData<>();

    public void init() {
        showProgressBar();

        LoginModel login = new LoginModel(USUARIO_WSR, SENHA_WSR);
        preparaLogin(login);

    }

    private void preparaLogin(LoginModel login) {
        String key = Auth.generateKey(LoginModel.getUsuario(), login.getPassword());
        LoginModel.setAuth("Basic " + new String(Auth.generateToken(key)));
        loginAction(LoginModel.getHeader());
    }

    private void loginAction(Map<String, String> header) {
        new LoginService().login(header, new IServiceCallback() {
            @Override
            public void onSuccessResponse(Call call, Response response) {
                LoginModel.setAuth(response.headers().get(Constantes.FLAG_AUTHORIZATION));
                LoginModel.setToken(response.headers().get(Constantes.FLAG_AUTHORIZATION));
                hideProgressBar();
            }

            @Override
            public void onErrorResponse(Call call, Throwable response) {
                message.setValue(response.getLocalizedMessage());
                response.printStackTrace();
                hideProgressBar();
            }
        });
    }

    public void validaMatricula(Matricula numeroMatricula) {
        showProgressBar();
        Log.i("numeroMatricula", numeroMatricula.getMatricula());
        new PickListService().validaMatricula(numeroMatricula.getMatricula(), LoginModel.getHeader(), new IServiceCallback<Boolean>() {
            @Override
            public void onSuccessResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.body() == null) {
                    hideProgressBar();
                    return;
                }

                if(response.body()) {
                    matriculaValida.setValue(true);
                } else {
                    if(numeroMatricula.getPrefixo().equals(CODIGO_CABO_FRIO)) {
                        numeroMatricula.setPrefixo(CODIGO_CABO_FRIO_MENSALISTA);    // Tem Horista e Mensalista em Cabo Frio, tem que tentar dos dois jeitos
                        validaMatricula(numeroMatricula);
                    } else {
                        hideProgressBar();
                        matriculaValida.setValue(false);
                    }
                }
            }

            @Override
            public void onErrorResponse(Call<Boolean> call, Throwable throwable) {
                showDialogoMatricula.setValue(false);
                matriculaValida.setValue(false);
                message.setValue(throwable.getLocalizedMessage());
            }
        });
    }

    public void abreDialogoMatricula() {
        showDialogoMatricula.setValue(true);
    }

    private void showProgressBar() {
        progressBar.setValue(true);
    }

    private void hideProgressBar() {
        progressBar.setValue(false);
    }

    public MutableLiveData<Boolean> getShowDialogoMatricula() {
        return showDialogoMatricula;
    }

    public MutableLiveData<Boolean> getMatriculaValida() {
        return matriculaValida;
    }

    public MutableLiveData<Boolean> getProgressBar() {
        return progressBar;
    }

    public MutableLiveData<String> getMessage() {
        return message;
    }
}
