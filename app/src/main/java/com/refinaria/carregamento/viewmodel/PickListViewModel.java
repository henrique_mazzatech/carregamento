package com.refinaria.carregamento.viewmodel;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.refinaria.carregamento.model.EstruturaPickListModel;
import com.refinaria.carregamento.model.LoginModel;
import com.refinaria.carregamento.model.ProdutosPickListModel;
import com.refinaria.carregamento.services.IServiceCallback;
import com.refinaria.carregamento.services.SharedPref;
import com.refinaria.carregamento.services.api.ApiModule;
import com.refinaria.carregamento.services.impl.PickListService;
import com.refinaria.carregamento.utils.DataUtils;
import com.refinaria.carregamento.utils.Matricula;
import com.refinaria.carregamento.utils.PickListUtils;
import com.refinaria.carregamento.utils.ScanUtils;

import retrofit2.Call;
import retrofit2.Response;

import static com.refinaria.carregamento.utils.Constantes.CODIGO_CABO_FRIO;
import static com.refinaria.carregamento.utils.Constantes.CODIGO_CABO_FRIO_MENSALISTA;
import static com.refinaria.carregamento.utils.Constantes.ERRO_NUMERO_PEDIDO_INVALIDO;
import static com.refinaria.carregamento.utils.Constantes.ERRO_TAMANHO_REMESSA_INVALIDO;
import static com.refinaria.carregamento.utils.Constantes.FLAG_BARCODE;
import static com.refinaria.carregamento.utils.Constantes.FLAG_FATURAMENTO;
import static com.refinaria.carregamento.utils.Constantes.FLAG_PEDIDO;
import static com.refinaria.carregamento.utils.Constantes.FLAG_REMESSA;
import static com.refinaria.carregamento.utils.Constantes.MATRICULA_INVALIDA;

public class PickListViewModel extends ViewModel {

    private MutableLiveData<Integer> camera = new MutableLiveData<>();
    private MutableLiveData<String> remessa = new MutableLiveData<>();
    private MutableLiveData<String> numeroPedido = new MutableLiveData<>();
    private MutableLiveData<ProdutosPickListModel> pickListMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<String> errorMessage = new MutableLiveData<>();
    private MutableLiveData<String> message = new MutableLiveData<>();
    private MutableLiveData<Boolean> progressBar = new MutableLiveData<>();
    private MutableLiveData<Boolean> tipoFaturamento = new MutableLiveData<>();
    private MutableLiveData<Boolean> dialogoMatricula = new MutableLiveData<>();
    private String matricula;

    public void init() {
        hideProgressBar();

        try {
            trataBarcode(SharedPref.read(FLAG_BARCODE, ""));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Requests
     ********************************************************************************************************/

    public void validaMatricula(Matricula numeroMatricula) {
        showProgressBar();
        new PickListService().validaMatricula(numeroMatricula.getMatricula(), LoginModel.getHeader(), new IServiceCallback<Boolean>() {
            @Override
            public void onSuccessResponse(Call<Boolean> call, Response<Boolean> response) {
                dialogoMatricula.setValue(false);

                if (response.body() == null) {
                    hideProgressBar();
                    return;
                }

                if(response.body()) {
                    matricula = numeroMatricula.getMatricula();
                    pickListAction();
                } else {
                    if(numeroMatricula.getPrefixo().equals(CODIGO_CABO_FRIO)) {
                        numeroMatricula.setPrefixo(CODIGO_CABO_FRIO_MENSALISTA);
                        validaMatricula(numeroMatricula);
                    } else {
                        hideProgressBar();
                        message.setValue(MATRICULA_INVALIDA);
                    }
                }
            }

            @Override
            public void onErrorResponse(Call<Boolean> call, Throwable throwable) {
                dialogoMatricula.setValue(false);
                message.setValue(throwable.getLocalizedMessage());
                hideProgressBar();
            }
        });
    }

    private void pickListAction() {
        salvaPedidoRemessa();

        EstruturaPickListModel estruturaPickListModel = montaEstruturaPickList();
        Log.i("pickList", new Gson().toJson(estruturaPickListModel));
        new PickListService().carregarPickList(LoginModel.getToken(), estruturaPickListModel, new IServiceCallback<ProdutosPickListModel>() {
            @Override
            public void onSuccessResponse(Call<ProdutosPickListModel> call, Response<ProdutosPickListModel> response) {
                Log.i("onSuccessResponse", new Gson().toJson(response));

                ProdutosPickListModel produtosPickListModel = ApiModule.getGsonInstance().fromJson(ApiModule.getGsonInstance().toJson(response.body()), ProdutosPickListModel.class);
                trataRetornoPickList(produtosPickListModel);
            }

            @Override
            public void onErrorResponse(Call<ProdutosPickListModel> call, Throwable throwable) {
                throwable.printStackTrace();
                message.setValue(throwable.getLocalizedMessage());
                hideProgressBar();
            }
        });
    }

    /**
     * Validações
     ************************************************************************************************************************/
    private void trataBarcode(String barcode) {
        if (!barcode.equals("")) {
            remessa.setValue(ScanUtils.getRemessa(barcode));
            numeroPedido.setValue(ScanUtils.getNumeroPedido(barcode));
        }
    }

    public void recebeBarcode(String barcode) {
        trataBarcode(barcode);
        onClickProduto();
    }

    public void onClickProduto() {
        if (!DataUtils.isCampoValido(remessa.getValue(), 5))
            errorMessage.setValue(ERRO_TAMANHO_REMESSA_INVALIDO);
        else if (!DataUtils.isCampoValido(numeroPedido.getValue(), 14))
            errorMessage.setValue(ERRO_NUMERO_PEDIDO_INVALIDO);
        else
            dialogoMatricula.setValue(true);
    }

    private void trataRetornoPickList(ProdutosPickListModel produtosPickListModel) {
        try {
            if (produtosPickListModel.getServiceRequest1().getFsP5542041W5542041B().getData().getGridData().getRowset().get(0).getZD20060().equals(" ")) {
                pickListMutableLiveData.setValue(produtosPickListModel);
            } else {
                hideProgressBar();
                message.setValue(produtosPickListModel.getServiceRequest1().getFsP5542041W5542041B().getData().getGridData().getRowset().get(0).getZD20060());
            }
        } catch (Exception e) {
            e.printStackTrace();
            message.setValue(e.getLocalizedMessage());
            hideProgressBar();
        }
    }

    private EstruturaPickListModel montaEstruturaPickList() {
        int faturamento = 0;
        if (tipoFaturamento.getValue() != null)
            faturamento = tipoFaturamento.getValue() ? 1 : 0;

        EstruturaPickListModel estruturaPickListModel = PickListUtils.montaEstruturaPickList(remessa.getValue(), numeroPedido.getValue(), faturamento);
        estruturaPickListModel.setMatricula(matricula);
        return estruturaPickListModel;
    }

    /*************************************************************************************************************************/

    private void salvaPedidoRemessa() {
        SharedPref.write(FLAG_REMESSA, remessa.getValue());
        SharedPref.write(FLAG_PEDIDO, numeroPedido.getValue());
        SharedPref.write(FLAG_FATURAMENTO, tipoFaturamento.getValue() != null ? tipoFaturamento.getValue() : false);
    }

    /**
     * ProgressBar
     ***********************************************************************************************************************/
    private void showProgressBar() {
        progressBar.setValue(true);
    }

    private void hideProgressBar() {
        progressBar.setValue(false);
    }

    /***********************************************************************************************************************/

    public void onClickCamera() {
        camera.setValue(1);
    }

    public MutableLiveData<String> getErrorMessage() {
        return errorMessage;
    }

    public MutableLiveData<Integer> getCamera() {
        return camera;
    }

    public MutableLiveData<String> getRemessa() {
        return remessa;
    }

    public MutableLiveData<String> getNumeroPedido() {
        return numeroPedido;
    }

    public MutableLiveData<ProdutosPickListModel> getPickListMutableLiveData() {
        return pickListMutableLiveData;
    }

    public MutableLiveData<Boolean> getProgressBar() {
        return progressBar;
    }

    public MutableLiveData<String> getMessage() {
        return message;
    }

    public MutableLiveData<Boolean> getTipoFaturamento() {
        return tipoFaturamento;
    }

    public MutableLiveData<Boolean> getDialogoMatricula() {
        return dialogoMatricula;
    }
}