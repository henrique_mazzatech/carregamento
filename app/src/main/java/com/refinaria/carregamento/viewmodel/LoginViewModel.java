package com.refinaria.carregamento.viewmodel;

import android.text.TextUtils;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.refinaria.carregamento.model.LoginModel;
import com.refinaria.carregamento.services.IServiceCallback;
import com.refinaria.carregamento.services.auth.Auth;
import com.refinaria.carregamento.services.impl.LoginService;
import com.refinaria.carregamento.utils.Constantes;
import com.refinaria.carregamento.utils.LoginUtils;
import com.refinaria.carregamento.utils.SingleLiveEvent;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;

public class LoginViewModel extends ViewModel {

    @SuppressWarnings("unused")
    private String TAG = getClass().getSimpleName();

    public MutableLiveData<String> usuario = new MutableLiveData<>();
    public MutableLiveData<String> password = new MutableLiveData<>();

    private MutableLiveData<LoginModel> userMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<Boolean> progressBar = new MutableLiveData<>();
    private MutableLiveData<String> errorMessage = new MutableLiveData<>();
    private MutableLiveData<String> toastMessage = new MutableLiveData<>();

    public SingleLiveEvent<Integer> getLoginAction() {
        return loginAction;
    }

    private SingleLiveEvent<Integer> loginAction = new SingleLiveEvent<>();

    public MutableLiveData<LoginModel> getUser() {
        if (userMutableLiveData == null)
            userMutableLiveData = new MutableLiveData<>();

        return userMutableLiveData;
    }

    public void init() {
        progressBar.setValue(false);
    }

    public void onClickLogin() {
        usuario.setValue("henrique.mazzatech");
        password.setValue("123mudar");

        LoginModel login = new LoginModel(usuario.getValue(), password.getValue());
        preparaLogin(login);
    }

    private void preparaLogin(LoginModel login) {


        if (!checkData(login)) return;

        progressBar.setValue(true);

        LoginModel.setAuth("Basic " + new String(Auth.generateToken(Auth.generateKey(login.getUsuario(), login.getPassword()))));
        userMutableLiveData.setValue(login);
        loginAction(LoginModel.getHeader());

    }

    private boolean checkData(LoginModel login) {
        if (TextUtils.isEmpty(login.getUsuario())) {
            errorMessage.setValue(Constantes.ERRO_DIGITE_UM_USUARIO);
            progressBar.setValue(false);
            return false;
        } else if (!login.isEmailValid()) {
            errorMessage.setValue(Constantes.ERRO_USUARIO_INVALIDO);
            progressBar.setValue(false);
            return false;
        } else if (!login.isPasswordValid()) {
            errorMessage.setValue(Constantes.ERRO_SENHA_INVALIDA);
            progressBar.setValue(false);
            return false;
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    private void loginAction(Map<String, String> header) {
        new LoginService().login(header, new IServiceCallback() {
            @Override
            public void onSuccessResponse(Call call, Response response) {
                loginAction.setValue(LoginUtils.trataRespostaLogin(response));
                LoginModel.setToken(response.headers().get(Constantes.FLAG_AUTHORIZATION));
            }

            @Override
            public void onErrorResponse(Call call, Throwable response) {
                response.printStackTrace();
                toastMessage.setValue(response.getLocalizedMessage());
                progressBar.setValue(false);
            }
        });
    }

    public MutableLiveData<Boolean> getProgressBar() {
        return progressBar;
    }

    public MutableLiveData<String> getErrorMessage() {
        return errorMessage;
    }

    public MutableLiveData<String> getToastMessage() {
        return toastMessage;
    }
}