package com.refinaria.carregamento.viewmodel;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.refinaria.carregamento.model.EstruturaPickListModel;
import com.refinaria.carregamento.model.LoginModel;
import com.refinaria.carregamento.model.ProdutosPickListModel;
import com.refinaria.carregamento.model.RepaletizacaoResponse;
import com.refinaria.carregamento.model.Rowset;
import com.refinaria.carregamento.services.IPickListService;
import com.refinaria.carregamento.services.IServiceCallback;
import com.refinaria.carregamento.services.SharedPref;
import com.refinaria.carregamento.services.impl.PickListService;
import com.refinaria.carregamento.utils.Constantes;
import com.refinaria.carregamento.utils.DataUtils;
import com.refinaria.carregamento.utils.RepaletizacaoUtils;
import com.refinaria.carregamento.utils.ScanUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

import static com.refinaria.carregamento.utils.Constantes.SHARED_COD_PROD;
import static com.refinaria.carregamento.utils.Constantes.SHARED_DOCUMENTO;
import static com.refinaria.carregamento.utils.Constantes.SHARED_QR_CODE;
import static com.refinaria.carregamento.utils.Constantes.TOAST_PRODUTO_REMOVIDO;
import static com.refinaria.carregamento.utils.RepaletizacaoUtils.montaRowsetViaQrCode;
import static com.refinaria.carregamento.utils.Utils.getCodigoFilial;

public class RepaletizacaoViewModel extends ViewModel {

    private IPickListService service = new PickListService();

    private MutableLiveData<String> numeroPallet = new MutableLiveData<>();
    private MutableLiveData<String> errorMessage = new MutableLiveData<>();
    private MutableLiveData<Boolean> progressBar = new MutableLiveData<>();
    private MutableLiveData<String> document = new MutableLiveData<>();
    private MutableLiveData<String> returnMessage = new MutableLiveData<>();
    private MutableLiveData<List<Rowset>> mutableProdutoList = new MutableLiveData<>();

    private List<Rowset> produtoList = new ArrayList<>();
    private String qrCode;
    private String codigoProduto = "";

    public void onClickAdd() {
        if (checkData()) {
            showProgressBar();
            EstruturaPickListModel estruturaPickListModel = RepaletizacaoUtils.estruturaAdicionarPallet(getCodigoFilial(), numeroPallet.getValue(), document.getValue());
            Log.i("estruturaPickListModel", new Gson().toJson(estruturaPickListModel));
            service.adicionarProdutoRepaletizacao(estruturaPickListModel, LoginModel.getToken(), new IServiceCallback<RepaletizacaoResponse>() {
                @Override
                public void onSuccessResponse(Call<RepaletizacaoResponse> call, Response<RepaletizacaoResponse> response) {
                    Log.i("onAdd", new Gson().toJson(response));
                    if (response.body() != null && response.body().getDoc() > 0 && response.body().getMensagem().equals(" ")) {
                        document.setValue(String.valueOf(response.body().getDoc()));
                        trataProduto();
                        salvaDados();
                    } else {
                        returnMessage.setValue(response.body() != null ? response.body().getMensagem() : null);
                    }

                    numeroPallet.setValue("");
                    hideProgressBar();
                }

                @Override
                public void onErrorResponse(Call<RepaletizacaoResponse> call, Throwable throwable) {
                    throwable.printStackTrace();
                    returnMessage.setValue(throwable.getLocalizedMessage());
                    hideProgressBar();
                }
            });
        }
    }

    private void salvaDados() {
        SharedPref.write(SHARED_COD_PROD , codigoProduto);
        SharedPref.write(SHARED_DOCUMENTO, document.getValue());
    }

    public void onClickCancel(int position) {
        showProgressBar();
        EstruturaPickListModel estruturaPickListModel = RepaletizacaoUtils.estruturaRemovePallet(getCodigoFilial(), numeroPallet.getValue(), document.getValue());
        service.adicionarProdutoRepaletizacao(estruturaPickListModel, LoginModel.getToken(), new IServiceCallback<ProdutosPickListModel>() {
            @Override
            public void onSuccessResponse(Call<ProdutosPickListModel> call, Response<ProdutosPickListModel> response) {
                produtoList.remove(position);
                mutableProdutoList.setValue(produtoList);
                returnMessage.setValue(TOAST_PRODUTO_REMOVIDO);
                hideProgressBar();
            }

            @Override
            public void onErrorResponse(Call<ProdutosPickListModel> call, Throwable throwable) {
                throwable.printStackTrace();
                returnMessage.setValue(throwable.getLocalizedMessage());
                hideProgressBar();
            }
        });
    }

    private void trataProduto() {
        Rowset produto = new Rowset();
        try {
            produto = montaRowsetViaQrCode(qrCode);
        } catch (Exception e) {
            produto.setZCNID59(numeroPallet.getValue());
            e.printStackTrace();
        }

        produtoList.add(produto);
        mutableProdutoList.setValue(produtoList);
    }

    private boolean checkData() {
        if (!DataUtils.isCampoValido(numeroPallet.getValue(), 5))
            errorMessage.setValue((Constantes.ERRO_TAMANHO_NUMERO_PALLET_INVALIDO));
        else {
            errorMessage.setValue("");
            return true;
        }
        return false;
    }

    public void bindingValidacaoNumeroPallet() {
        errorMessage.setValue(ScanUtils.validaNumeroPallet(numeroPallet.getValue()));
    }

    public MutableLiveData<String> getNumeroPallet() {
        return numeroPallet;
    }

    private void showProgressBar() {
        progressBar.setValue(true);
    }

    private void hideProgressBar() {
        progressBar.setValue(false);
    }

    public void recebeQrCode(String qrCode) {
        trataQrCode(qrCode);
        onClickAdd();
        this.qrCode = qrCode;

        codigoProduto = ScanUtils.getCodigoProduto(qrCode);

        SharedPref.write(SHARED_QR_CODE ,  qrCode);
        salvaDados();
    }

    private void trataQrCode(String qrCode) {
        numeroPallet.setValue(qrCode.isEmpty() ? "" : ScanUtils.getNumeroPalete(qrCode));
    }

    public MutableLiveData<String> getErrorMessage() {
        return errorMessage;
    }

    public MutableLiveData<Boolean> getProgressBar() {
        return progressBar;
    }

    public MutableLiveData<String> getReturnMessage() {
        return returnMessage;
    }

    public MutableLiveData<List<Rowset>> getMutableProdutoList() {
        return mutableProdutoList;
    }
}