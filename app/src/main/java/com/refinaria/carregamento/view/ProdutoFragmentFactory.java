package com.refinaria.carregamento.view;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.refinaria.carregamento.model.ProdutosPickListModel;
import com.refinaria.carregamento.services.api.ApiModule;
import com.refinaria.carregamento.viewmodel.ProdutoViewModel;

public class ProdutoFragmentFactory implements ViewModelProvider.Factory {

    private ProdutosPickListModel produtosPickListModel;

    public ProdutoFragmentFactory(ProdutosPickListModel produtosPickListModel) {
        Log.i("ProdutoFragmentFactory", ApiModule.getGsonInstance().toJson(produtosPickListModel));

        this.produtosPickListModel = produtosPickListModel;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        Log.i("create", ApiModule.getGsonInstance().toJson(produtosPickListModel));

        return (T) new ProdutoViewModel(produtosPickListModel);
    }
}
