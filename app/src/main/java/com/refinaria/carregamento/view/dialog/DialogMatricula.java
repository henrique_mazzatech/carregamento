package com.refinaria.carregamento.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.refinaria.carregamento.R;
import com.refinaria.carregamento.services.SharedPref;
import com.refinaria.carregamento.utils.Matricula;
import com.refinaria.carregamento.view.IValidaMatricula;

import static com.refinaria.carregamento.utils.Constantes.CABO_FRIO;
import static com.refinaria.carregamento.utils.Constantes.CODIGO_CABO_FRIO;
import static com.refinaria.carregamento.utils.Constantes.CODIGO_GUARULHOS;
import static com.refinaria.carregamento.utils.Constantes.FILIAL;
import static com.refinaria.carregamento.utils.Constantes.GUARULHOS;

@SuppressWarnings("FieldCanBeLocal")
public class DialogMatricula extends Dialog {

    private IValidaMatricula iValidaMatricula;
    private String LOCAL = "";
    private static Dialog dialog;

    public DialogMatricula(IValidaMatricula iValidaMatricula, Context context) {
        super(context);
        this.iValidaMatricula = iValidaMatricula;

        dialog = new Dialog(context);
        init();
    }

    private void init() {
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_matricula);

        RadioGroup radioGroup = dialog.findViewById(R.id.radio_group_filial);
        if (SharedPref.read(FILIAL, CABO_FRIO).equals(CABO_FRIO)) {
            radioGroup.check(R.id.radio_button_cabo_frio);
            LOCAL = CODIGO_CABO_FRIO;
        } else {
            radioGroup.check(R.id.radio_button_guarulhos);
            LOCAL = CODIGO_GUARULHOS;
        }

        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            RadioButton rb = dialog.findViewById(checkedId);
            if (rb.getText().equals(CABO_FRIO)) {
                LOCAL = CODIGO_CABO_FRIO;
                SharedPref.write(FILIAL, CABO_FRIO);
            } else {
                LOCAL = CODIGO_GUARULHOS;
                SharedPref.write(FILIAL, GUARULHOS);
            }
        });

        Button dialogButton = dialog.findViewById(R.id.buttonOk);
        dialogButton.setOnClickListener(v -> {
            iValidaMatricula.onOkClicked(new Matricula(LOCAL, ((TextView) dialog.findViewById(R.id.editTextNumeroMatricula)).getText().toString()));
            dialog.dismiss();
        });
    }

    @Override
    public void show() {
        if (!dialog.isShowing())
            dialog.show();
    }
}