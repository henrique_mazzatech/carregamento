package com.refinaria.carregamento.view.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.refinaria.carregamento.R;
import com.refinaria.carregamento.databinding.ActivityConferirPedidoBinding;
import com.refinaria.carregamento.model.Rowset;
import com.refinaria.carregamento.services.api.ApiModule;
import com.refinaria.carregamento.utils.GeneralDialogFragment;
import com.refinaria.carregamento.view.ConferirPedidoFragmentFactory;
import com.refinaria.carregamento.view.adapters.ConferirPedidoRecyclerViewAdapter;
import com.refinaria.carregamento.viewmodel.ConferirPedidoViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ConferirPedidoActivity extends GenericActivity {

    private ConferirPedidoViewModel conferirPedidoViewModel;
    private Rowset[] produtosPickListModel;
    private ActivityConferirPedidoBinding binding;
    private ConferirPedidoRecyclerViewAdapter conferirPedidoAdapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_conferir_pedido);
        binding.setLifecycleOwner(this);

        binding.editTextRemessa.setEnabled(false);
        binding.editTextNumeroPedido.setEnabled(false);
        //binding.editTextFaturamento.setEnabled(false);

        produtosPickListModel = ApiModule.getGsonInstance().fromJson(Objects.requireNonNull(getIntent().getExtras()).getString("produtos"), Rowset[].class);

        setupViewModel();
        setupListAdapter();
    }

    private void setupViewModel() {
        ConferirPedidoFragmentFactory factory = new ConferirPedidoFragmentFactory(produtosPickListModel);
        conferirPedidoViewModel = ViewModelProviders.of(this, factory).get(ConferirPedidoViewModel.class);
        conferirPedidoViewModel.getPedidoList().observe(this, this::trataProdutoList);
        conferirPedidoViewModel.getPedidoListInicial().observe(this, produtosPickListModel -> {
        });
        conferirPedidoViewModel.getMensagemConfere().observe(this, this::exibeMensagemErro);
        conferirPedidoViewModel.getProgressBar().observe(this, this::trataProgressBar);
        conferirPedidoViewModel.getTipoFaturamento().observe(this, this::trataTipoFaturamento);


        binding.setConferirPedidoViewModel(conferirPedidoViewModel);
    }

    private void trataTipoFaturamento(Boolean aBoolean) {
        if (aBoolean) {
            binding.editTextRemessa.setTextColor(getResources().getColor(R.color.colorFaturaAccent));
            binding.editTextNumeroPedido.setTextColor(getResources().getColor(R.color.colorFaturaAccent));
        } else {
            binding.editTextRemessa.setTextColor(getResources().getColor(R.color.gray));
            binding.editTextNumeroPedido.setTextColor(getResources().getColor(R.color.gray));
        }
    }

    private void trataProgressBar(Boolean aBoolean) {
        if (aBoolean) {
            binding.progressCircular.setIndeterminate(true);
            binding.layoutProgress.setVisibility(View.VISIBLE);
        } else binding.layoutProgress.setVisibility(View.INVISIBLE);
    }

    private void exibeMensagemErro(String s) {
        if (s.contains("confere com o total")) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            View layoutView = getLayoutInflater().inflate(R.layout.dialog_postive_layout, null);
            dialogBuilder.setView(layoutView);
            AlertDialog alertDialog = dialogBuilder.create();
            Objects.requireNonNull(alertDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            Objects.requireNonNull(alertDialog.getWindow()).getAttributes().windowAnimations = R.style.DialogAnimation;
            alertDialog.show();

            alertDialog.setOnDismissListener(dialogInterface -> {

                Intent intent = new Intent(this, PickListActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            });
        } else {
            GeneralDialogFragment.dialogoFalhaConferir(this, s);
        }
    }

    private void setupListAdapter() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        binding.conferirPedidoRecyclerView.setLayoutManager(layoutManager);
        binding.conferirPedidoRecyclerView.addItemDecoration(new DividerItemDecoration(Objects.requireNonNull(this), DividerItemDecoration.VERTICAL));

        conferirPedidoAdapter = new ConferirPedidoRecyclerViewAdapter(new ArrayList<>(), this, conferirPedidoViewModel);
        binding.conferirPedidoRecyclerView.setAdapter(conferirPedidoAdapter);
    }

    private void trataProdutoList(List<Rowset> produtos) {
        conferirPedidoAdapter.updateList(produtos);
        conferirPedidoAdapter.notifyDataSetChanged();
    }
}
