package com.refinaria.carregamento.view;

import android.app.Application;

import com.refinaria.carregamento.services.api.ApiModule;

public class ExpedicaoApplication extends Application {

    private String URL_TESTE = "http://192.168.74.51:8099/";
    //private String URL_SERVIDOR = "http://192.168.2.10:8099/";
    private String URL_SERVIDOR = "http://salcisne.info:8099/";
    private String URL_LOCAL = "http://192.168.74.174:8099/";

    @Override
    public void onCreate() {
        super.onCreate();

        new ApiModule(URL_SERVIDOR);

    }

}
