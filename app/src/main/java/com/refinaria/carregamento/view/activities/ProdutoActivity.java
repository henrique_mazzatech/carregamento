package com.refinaria.carregamento.view.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.refinaria.carregamento.R;
import com.refinaria.carregamento.databinding.ActivityProdutoBinding;
import com.refinaria.carregamento.model.ProdutosPickListModel;
import com.refinaria.carregamento.model.Rowset;
import com.refinaria.carregamento.services.ScannerService;
import com.refinaria.carregamento.services.api.ApiModule;
import com.refinaria.carregamento.utils.Utils;
import com.refinaria.carregamento.view.ProdutoFragmentFactory;
import com.refinaria.carregamento.view.adapters.ProdutoAdapter;
import com.refinaria.carregamento.viewmodel.ProdutoViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ProdutoActivity extends GenericActivity implements GenericActivity.IOnReceiveScan {

    private ActivityProdutoBinding binding;
    private ProdutosPickListModel produtosPickListModel;
    private ProdutoViewModel produtoViewModel;
    private ProdutoAdapter produtoAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_produto);
        binding.setLifecycleOwner(this);

        produtosPickListModel = ApiModule.getGsonInstance().fromJson(Objects.requireNonNull(getIntent().getExtras()).getString("produtos"), ProdutosPickListModel.class);

        setupViewModel();
        setupListAdapter();

        receiver = new ScanReceiver(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        binding.editTextRemessa.setEnabled(false);
        binding.editTextNumeroPedido.setEnabled(false);
        //binding.editTextFaturamento.setEnabled(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        produtoViewModel.init();
    }

    private void setupListAdapter() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        binding.produtoRecyclerView.setLayoutManager(layoutManager);
        binding.produtoRecyclerView.addItemDecoration(new DividerItemDecoration(Objects.requireNonNull(this), DividerItemDecoration.VERTICAL));

        produtoAdapter = new ProdutoAdapter(new ArrayList<>(), this, position -> produtoViewModel.onCancelProduto(position));
        binding.produtoRecyclerView.setAdapter(produtoAdapter);
    }

    private void setupViewModel() {
        ProdutoFragmentFactory produtoFragmentFactory = new ProdutoFragmentFactory(produtosPickListModel);
        produtoViewModel = ViewModelProviders.of(this, produtoFragmentFactory).get(ProdutoViewModel.class);
        binding.setProdutoViewModel(produtoViewModel);

        produtoViewModel.getCamera().observe(this, this::abreCamera);
        produtoViewModel.getCheckedQuantidadeFracionada().observe(this, this::handleCheckQuantidadeFracionada);
        produtoViewModel.getErrorMessage().observe(this, this::trataErro);
        produtoViewModel.getRetornoMessage().observe(this, s -> Utils.longToast(this, s));
        produtoViewModel.getMutableProdutoList().observe(this, this::trataProdutoList);
        produtoViewModel.getProgressBar().observe(this, this::trataProgressBar);
        produtoViewModel.getConferirProdutos().observe(this, this::handleOnClickConferir);
        produtoViewModel.getTipoFaturamento().observe(this, this::trataTipoFaturamento);
    }

    private void trataTipoFaturamento(Boolean aBoolean) {
        if(aBoolean) {
            binding.editTextRemessa.setTextColor(getResources().getColor(R.color.colorFaturaAccent));
            binding.editTextNumeroPedido.setTextColor(getResources().getColor(R.color.colorFaturaAccent));
        } else {
            binding.editTextRemessa.setTextColor(getResources().getColor(R.color.gray));
            binding.editTextNumeroPedido.setTextColor(getResources().getColor(R.color.gray));
        }
    }

    private void handleOnClickConferir(List<Rowset> rowsetArray) {
        Intent intent = new Intent(this, ConferirPedidoActivity.class);
        intent.putExtra("produtos", ApiModule.getGsonInstance().toJson(rowsetArray));
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void trataProgressBar(Boolean aBoolean) {
        if (aBoolean) {
            binding.progressCircular.setIndeterminate(true);
            binding.layoutProgress.setVisibility(View.VISIBLE);
        } else binding.layoutProgress.setVisibility(View.INVISIBLE);
    }

    private void trataProdutoList(List<Rowset> produtos) {
        // Pra ordenar com o item mais recente no topo da lista

        produtoAdapter.updateList(produtos);
        produtoAdapter.notifyDataSetChanged();
    }

    private void trataErro(String erro) {
        if (erro.contains("remessa")) {
            binding.editTextRemessa.setError(erro);
            binding.editTextRemessa.requestFocus();
        } else if (erro.contains("produto")) {
            binding.editTextNumeroPedido.setError(erro);
            binding.editTextNumeroPedido.requestFocus();
        } else if (erro.contains("palete")) {
            binding.editTextNumeroPalete.setError(erro);
            binding.editTextNumeroPalete.requestFocus();
        } else if (erro.equals("")) {
            binding.editTextNumeroPedido.setError(null);
            binding.editTextRemessa.setError(null);
            binding.editTextNumeroPalete.setError(null);
        }
    }

    private void handleCheckQuantidadeFracionada(boolean checked) {
        if (!checked) {
            binding.editTextQuantidadeFracionada.getText().clear();
            binding.editTextQuantidadeFracionada.clearFocus();
        }
        binding.editTextQuantidadeFracionada.setEnabled(checked);
    }

    private void abreCamera(boolean aux) {
        if (aux) {
            Intent intent = new Intent(this, ScanQrCodeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }

    @Override
    public void received(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            String qrCode = bundle.getString(ScannerService.BAR_CODE);
            log(
                    qrCode
                            + bundle.getString(ScannerService.CODE_TYPE)
                            + bundle.getInt(ScannerService.LENGTH) + "\n");

            try {
                if (qrCode != null)
                    produtoViewModel.recebeQrCode(qrCode);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}