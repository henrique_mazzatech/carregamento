package com.refinaria.carregamento.view.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.refinaria.carregamento.R;
import com.refinaria.carregamento.model.clientes.Rowset;
import com.refinaria.carregamento.services.SharedPref;

import java.util.ArrayList;
import java.util.List;

import static com.refinaria.carregamento.utils.Constantes.SHARED_COD_CLIENTE;

public class ClientesAdapter extends RecyclerView.Adapter<ClientesAdapter.ClienteViewHolder> {

    private List<Rowset> rowsetList;
    private int selectedPosition = -1;
    private Context context;

    public ClientesAdapter(List<Rowset> rowsetList, Context context) {
        this.rowsetList = rowsetList;
        this.context = context;
    }

    class ClienteViewHolder extends RecyclerView.ViewHolder {
        private TextView nomeCliente;
        private TextView quantidade;
        private TextView titleQtd;

        ClienteViewHolder(@NonNull View itemView) {
            super(itemView);
            nomeCliente = itemView.findViewById(R.id.nomeCliente);
            quantidade = itemView.findViewById(R.id.quantidade);
            titleQtd = itemView.findViewById(R.id.titleQtd);
        }
    }

    @NonNull
    @Override
    public ClienteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ClienteViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.cliente, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ClienteViewHolder holder, int position) {
        Rowset rowset = rowsetList.get(position);

        if (selectedPosition == position) {
            holder.itemView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
            holder.nomeCliente.setTextColor(ContextCompat.getColor(context, R.color.white));
            holder.quantidade.setTextColor(ContextCompat.getColor(context, R.color.white));
            holder.titleQtd.setTextColor(ContextCompat.getColor(context, R.color.white));
        } else {
            holder.nomeCliente.setTextColor(ContextCompat.getColor(context, R.color.dark_gray));
            holder.quantidade.setTextColor(ContextCompat.getColor(context, R.color.dark_gray));
            holder.titleQtd.setTextColor(ContextCompat.getColor(context, R.color.dark_gray));
            if (position % 2 != 0) {
                holder.itemView.setBackgroundColor(Color.TRANSPARENT);
            } else {
                holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.light_gray));
            }
        }

        holder.itemView.setOnClickListener(v -> {
            selectedPosition = position;
            notifyDataSetChanged();
            SharedPref.write(SHARED_COD_CLIENTE, rowset.getCodCliente());
        });

        holder.nomeCliente.setText(capitalize(rowset.getCliente()));
        holder.quantidade.setText(rowset.getQtdePallet());
    }

    @Override
    public int getItemCount() {
        return rowsetList != null ? rowsetList.size() : 0;
    }

    private String capitalize(String text) {
        text = text.toLowerCase();
        String[] strArray = text.split(" ");
        StringBuilder builder = new StringBuilder();

        for (String s : strArray) {
            String cap = s.substring(0, 1).toUpperCase() + s.substring(1);
            builder.append(cap).append(" ");
        }

        return builder.toString();
    }

    private void insertProduto(List<Rowset> rowset) {
        rowsetList.clear();
        rowsetList.addAll(rowset);
    }

    public void updateList(ArrayList<Rowset> rowset) {
        insertProduto(rowset);
    }

}