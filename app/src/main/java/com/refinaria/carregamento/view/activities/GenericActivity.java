package com.refinaria.carregamento.view.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.media.RingtoneManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.refinaria.carregamento.services.ScannerService;
import com.refinaria.carregamento.services.SharedPref;
import com.seuic.scanner.Scanner;
import com.seuic.scanner.ScannerFactory;

public abstract class GenericActivity extends AppCompatActivity {

    Scanner scanner;
    ScanReceiver receiver;
    IntentFilter filter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        startService(new Intent(this, ScannerService.class));
        setupBarcodeReader();

        SharedPref.init(this);
    }

    private void setupBarcodeReader() {
        scanner = ScannerFactory.getScanner(this);

        filter = new IntentFilter(ScannerService.ACTION);

        scanner = ScannerFactory.getScanner(this);
        if (scanner == null)
            log("scanner(NULL)");
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Register the receiver
        if (receiver != null)
            registerReceiver(receiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Unregister the receiver
        if (receiver != null)
            unregisterReceiver(receiver);
    }

    void log(String string) {
        Log.i("Picklist", string);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    assert imm != null;
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    public class ScanReceiver extends BroadcastReceiver {
        IOnReceiveScan iOnReceive;
        ScanReceiver(IOnReceiveScan onReceive) {
            this.iOnReceive = onReceive;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            final ToneGenerator tg = new ToneGenerator(RingtoneManager.TYPE_NOTIFICATION, 100);
            tg.startTone(ToneGenerator.TONE_PROP_BEEP);

            iOnReceive.received(context, intent);
        }
    }

    public interface IOnReceiveScan {
        void received(Context context, Intent intent);
    }
}
