package com.refinaria.carregamento.view.activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.widget.CompoundButtonCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.refinaria.carregamento.R;
import com.refinaria.carregamento.databinding.ActivityPickListBinding;
import com.refinaria.carregamento.model.ProdutosPickListModel;
import com.refinaria.carregamento.services.ScannerService;
import com.refinaria.carregamento.services.SharedPref;
import com.refinaria.carregamento.services.api.ApiModule;
import com.refinaria.carregamento.utils.Matricula;
import com.refinaria.carregamento.utils.Utils;
import com.refinaria.carregamento.view.IValidaMatricula;
import com.refinaria.carregamento.view.dialog.DialogMatricula;
import com.refinaria.carregamento.viewmodel.PickListViewModel;

import static com.refinaria.carregamento.utils.Constantes.FILIAL;
import static com.refinaria.carregamento.utils.Constantes.FLAG_BARCODE;
import static com.refinaria.carregamento.utils.Constantes.FLAG_QR_CODE;

public class PickListActivity extends GenericActivity implements GenericActivity.IOnReceiveScan, IValidaMatricula {

    private ActivityPickListBinding binding;
    private PickListViewModel pickListViewModel;
    private DialogMatricula dialogMatricula;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_pick_list);
        binding.setLifecycleOwner(this);

        binding.cameraButton.setVisibility(View.GONE);
        CompoundButtonCompat.setButtonTintList(binding.checkBox, ColorStateList.valueOf(ContextCompat.getColor(this, R.color.gray)));

        SharedPref.remove(FLAG_BARCODE);
        setupViewModel();

        receiver = new ScanReceiver(this);
    }

    void log(String string) {
        Log.i("PickListActivity", string);
    }

    @Override
    public void onResume() {
        super.onResume();
        pickListViewModel.init();
        SharedPref.remove(FLAG_QR_CODE);
    }

    public void dialogOnClick(View view) {
        SharedPref.write(FILIAL, (String) ((RadioButton) view).getText());
    }

    private void setupViewModel() {
        pickListViewModel = ViewModelProviders.of(this).get(PickListViewModel.class);
        binding.setPickListViewModel(pickListViewModel);

        pickListViewModel.getCamera().observe(this, aux -> abreCamera());
        pickListViewModel.getPickListMutableLiveData().observe(this, this::startProdutoFragment);
        pickListViewModel.getErrorMessage().observe(this, this::trataErro);
        pickListViewModel.getProgressBar().observe(this, this::trataProgressBar);
        pickListViewModel.getMessage().observe(this, this::trataMensagem);
        pickListViewModel.getTipoFaturamento().observe(this, this::trataFaturamento);
        pickListViewModel.getDialogoMatricula().observe(this, this::abreMatricula);
    }

    private void abreMatricula(Boolean aBoolean) {
        if (!aBoolean) return;

        if (dialogMatricula == null)
            dialogMatricula = new DialogMatricula(this, this);

        dialogMatricula.show();
    }

    private void trataFaturamento(Boolean aBoolean) {
        if (aBoolean) {
            binding.checkBox.setHintTextColor(ContextCompat.getColor(this, R.color.colorFatura));
            CompoundButtonCompat.setButtonTintList(binding.checkBox, ColorStateList.valueOf(ContextCompat.getColor(this, R.color.colorFatura)));
        } else {
            binding.checkBox.setHintTextColor(ContextCompat.getColor(this, R.color.gray));
            CompoundButtonCompat.setButtonTintList(binding.checkBox, ColorStateList.valueOf(ContextCompat.getColor(this, R.color.gray)));
        }

        binding.checkBox.setChecked(aBoolean);
    }

    private void trataMensagem(String s) {
        Utils.longToast(this, s);
    }

    private void trataErro(String erro) {
        if (erro.contains("remessa")) {
            binding.editTextRemessa.setError(erro);
            binding.editTextRemessa.requestFocus();
        } else if (erro.contains("pedido")) {
            binding.editTextNumeroPedido.setError(erro);
            binding.editTextNumeroPedido.requestFocus();
        }
    }

    private void abreCamera() {
    }

    private void startProdutoFragment(ProdutosPickListModel produtosPickListModel) {
        Intent intent = new Intent(this, ProdutoActivity.class);
        intent.putExtra("produtos", ApiModule.getGsonInstance().toJson(produtosPickListModel));
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void trataProgressBar(Boolean aBoolean) {
        if (aBoolean) {
            binding.progressCircular.setIndeterminate(true);
            binding.layoutProgress.setVisibility(View.VISIBLE);
        } else binding.layoutProgress.setVisibility(View.INVISIBLE);
    }

    @Override
    public void received(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            String barcode = bundle.getString(ScannerService.BAR_CODE);
            log(barcode
                    + bundle.getString(ScannerService.CODE_TYPE)
                    + bundle.getInt(ScannerService.LENGTH) + "\n"
            );

            try {
                if (barcode != null)
                    pickListViewModel.recebeBarcode(barcode);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onOkClicked(Matricula matricula) {
        pickListViewModel.validaMatricula(matricula);
    }
}