package com.refinaria.carregamento.view;

import com.refinaria.carregamento.utils.Matricula;

public interface IValidaMatricula {
    void onOkClicked(Matricula matricula);
}
