package com.refinaria.carregamento.view.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.refinaria.carregamento.R;
import com.refinaria.carregamento.model.Rowset;
import com.refinaria.carregamento.viewmodel.ConferirPedidoViewModel;

import java.util.List;

import static com.refinaria.carregamento.utils.Utils.firstUppercase;

public class ConferirPedidoRecyclerViewAdapter extends RecyclerView.Adapter<ConferirPedidoRecyclerViewAdapter.ProdutoViewHolder> {

    private List<Rowset> rowsetList;
    private Context context;
    private ConferirPedidoViewModel conferirPedidoViewModel;

    public ConferirPedidoRecyclerViewAdapter(List<Rowset> rowsetList, Context context, ConferirPedidoViewModel conferirPedidoViewModel) {
        this.rowsetList = rowsetList;
        this.context = context;
        this.conferirPedidoViewModel = conferirPedidoViewModel;
    }

    class ProdutoViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView numeroProdutoTextView;
        private TextView totalTextView;

        ProdutoViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.produtoImageView);
            numeroProdutoTextView = itemView.findViewById(R.id.produtoTextView);
            totalTextView = itemView.findViewById(R.id.totalTextView);
        }
    }

    @NonNull
    @Override
    public ProdutoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ProdutoViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.conferir_pedido, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ProdutoViewHolder holder, int position) {
        Rowset rowset = rowsetList.get(position);
        String titulo = firstUppercase (rowset.getZLITM64() + ": " + rowset.getZDSC162());

        int imageId = escolheImagemProduto(rowset);
        holder.imageView.setBackgroundResource(imageId);
        holder.numeroProdutoTextView.setText(titulo);
        holder.totalTextView.setText(Integer.toString(rowset.getZQNTA68()));
    }

    private int escolheImagemProduto(Rowset rowset) {
        switch (rowset.getZLITM64()) {
            case "0801":
                return R.drawable.produto_0801;
            case "0802":
                return R.drawable.produto_0802;
            case "0803":
                return R.drawable.produto_0803;
            case "7013":
                return R.drawable.produto_7013;
            case "7021":
                return R.drawable.produto_7021;
            case "7080":
                return R.drawable.produto_7080;
            case "7102":
                return R.drawable.produto_7102;
            case "7110-P":
                return R.drawable.produto_7110p;
            case "7110":
                return R.drawable.produto_7110;
            case "7129":
                return R.drawable.produto_7129;
            case "7137":
                return R.drawable.produto_7137;
            case "7153":
                return R.drawable.produto_7153;
            case "7161":
                return R.drawable.produto_7161;
            case "7809":
                return R.drawable.produto_7809;
            case "7810":
                return R.drawable.produto_7810;
            case "7812":
                return R.drawable.produto_7812;
            default:
                return R.drawable.cisne_logo;
        }
    }

    @Override
    public int getItemCount() {
        return rowsetList != null ? rowsetList.size() : 0;
    }

    private void insertProduto(List<Rowset> rowset) {
        rowsetList.clear();
        rowsetList.addAll(rowset);
    }

    public void updateList(List<Rowset> rowset) {
        insertProduto(rowset);
    }
}