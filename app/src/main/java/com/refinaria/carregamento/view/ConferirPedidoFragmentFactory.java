package com.refinaria.carregamento.view;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.refinaria.carregamento.model.Rowset;
import com.refinaria.carregamento.viewmodel.ConferirPedidoViewModel;

public class ConferirPedidoFragmentFactory implements ViewModelProvider.Factory {

    private Rowset[] produtosPickListModel;

    public ConferirPedidoFragmentFactory(Rowset[] produtosPickListModel) {
        this.produtosPickListModel = produtosPickListModel;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new ConferirPedidoViewModel(produtosPickListModel);
    }
}
