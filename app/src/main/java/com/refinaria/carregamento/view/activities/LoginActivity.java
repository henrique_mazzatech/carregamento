package com.refinaria.carregamento.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.refinaria.carregamento.R;
import com.refinaria.carregamento.databinding.ActivityLoginBinding;
import com.refinaria.carregamento.services.ScannerService;
import com.refinaria.carregamento.services.SharedPref;
import com.refinaria.carregamento.utils.GeneralDialogFragment;
import com.refinaria.carregamento.utils.Utils;
import com.refinaria.carregamento.viewmodel.LoginViewModel;

import java.util.Objects;

import static com.refinaria.carregamento.utils.Constantes.ERRO_DIGITE_UM_USUARIO;
import static com.refinaria.carregamento.utils.Constantes.ERRO_SENHA_INVALIDA;
import static com.refinaria.carregamento.utils.Constantes.ERRO_USUARIO_INVALIDO;
import static com.refinaria.carregamento.utils.Constantes.FLAG_USUARIO;

public class LoginActivity extends GenericActivity {

    public ActivityLoginBinding binding;

    @SuppressWarnings("unused")
    private String TAG = getClass().getSimpleName();
    LoginViewModel loginViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupViewModel();

        // Call the service when the program is opened, you can call only once, do not need to call in each activity
        Intent intent = new Intent(this, ScannerService.class);
        startService(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loginViewModel.init();
    }

    private void setupViewModel() {
        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);

        binding = DataBindingUtil.setContentView(LoginActivity.this, R.layout.activity_login);
        binding.setLifecycleOwner(this);
        binding.setLoginViewModel(loginViewModel);

        loginViewModel.getErrorMessage().observe(this, this::trataErro);
        loginViewModel.getProgressBar().observe(this, this::trataProgressBar);
        loginViewModel.getToastMessage().observe(this, s -> Utils.longToast(this, s));

        loginViewModel.getLoginAction().observe(this, status -> {
            switch (status) {
                case 1:
                    usuarioAutorizado(Objects.requireNonNull(loginViewModel.getUser().getValue()).getUsuario());
                    break;
                case 2:
                    usuarioNaoAutorizado();
                    binding.layoutProgress.setVisibility(View.INVISIBLE);
                    break;
                default:
                    erroInesperado();
                    binding.layoutProgress.setVisibility(View.VISIBLE);
                    break;
            }
        });
    }

    private void trataErro(String erro) {
        if (erro.equals(ERRO_DIGITE_UM_USUARIO)) {
            binding.editTextEmail.setError(erro);
            binding.editTextEmail.requestFocus();
        } else if (erro.equals(ERRO_USUARIO_INVALIDO)) {
            binding.editTextEmail.setError(erro);
            binding.editTextEmail.requestFocus();
        } else if (erro.equals(ERRO_SENHA_INVALIDA)) {
            binding.editTextPassword.setError(erro);
            binding.editTextPassword.requestFocus();
        }
    }

    private void trataProgressBar(Boolean aBoolean) {
        if (aBoolean) {
            binding.progressCircular.setIndeterminate(true);
            binding.layoutProgress.setVisibility(View.VISIBLE);
        } else binding.layoutProgress.setVisibility(View.INVISIBLE);
    }

    private void erroInesperado() {
        GeneralDialogFragment.dialogoErroInesperado(getApplication());
    }

    private void usuarioNaoAutorizado() {
        GeneralDialogFragment.dialogoNaoAutorizado(this);
    }

    private void usuarioAutorizado(String usuario) {
        salvaUsuario(usuario);
        Intent intent = new Intent(this, PickListActivity.class);
        startActivity(intent);
        finish();
    }

    private void salvaUsuario(String usuario) {
        SharedPref.init(getApplicationContext());
        SharedPref.write(FLAG_USUARIO, usuario);
    }

}