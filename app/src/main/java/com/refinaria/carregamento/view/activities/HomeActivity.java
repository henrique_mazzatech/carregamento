package com.refinaria.carregamento.view.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.refinaria.carregamento.R;
import com.refinaria.carregamento.databinding.ActivityHomeBinding;
import com.refinaria.carregamento.services.SharedPref;
import com.refinaria.carregamento.utils.Matricula;
import com.refinaria.carregamento.utils.Utils;
import com.refinaria.carregamento.view.IValidaMatricula;
import com.refinaria.carregamento.view.dialog.DialogMatricula;
import com.refinaria.carregamento.viewmodel.HomeViewModel;

import static com.refinaria.carregamento.utils.Constantes.FILIAL;
import static com.refinaria.carregamento.utils.Constantes.MATRICULA_INVALIDA;

public class HomeActivity extends GenericActivity implements IValidaMatricula {

    private HomeViewModel homeViewModel;
    private ActivityHomeBinding binding;
    private DialogMatricula dialogMatricula;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        binding.setLifecycleOwner(this);

        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            binding.versaoApp.setText(pInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        setupViewModel();
    }

    @Override
    protected void onResume() {
        super.onResume();
        homeViewModel.init();
    }

    private void setupViewModel() {

        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        binding.setHomeViewModel(homeViewModel);

        homeViewModel.getShowDialogoMatricula().observe(this, this::abreMatricula);
        homeViewModel.getMatriculaValida().observe(this, this::matriculaValida);
        homeViewModel.getProgressBar().observe(this, this::trataProgressBar);
        homeViewModel.getMessage().observe(this, s -> Utils.longToast(this, s));
    }

    private void trataProgressBar(Boolean aBoolean) {
        if (aBoolean) {
            binding.progressCircular.setIndeterminate(true);
            binding.layoutProgress.setVisibility(View.VISIBLE);
            binding.layoutProgress.setFocusable(false);

        } else binding.layoutProgress.setVisibility(View.INVISIBLE);
    }

    private void matriculaValida(Boolean aBoolean) {
        if (aBoolean)
            onRepaletizacaoClicked();
        else
            Toast.makeText(this, MATRICULA_INVALIDA, Toast.LENGTH_SHORT).show();
    }


    public void onCarregamentoClicked(View view) {
        startActivity(new Intent(this, PickListActivity.class));
    }

    public void onRepaletizacaoClicked() {
        startActivity(new Intent(this, RepaletizacaoActivity.class));
    }

    public void onConsultarSaldoClicked(View view) {
        startActivity(new Intent(this, ConsultaSaldoActivity.class));
    }

    private void abreMatricula(Boolean aBoolean) {
        if (!aBoolean) return;

        dialogMatricula = new DialogMatricula(this, this);

        if (!this.isFinishing()) {
            dialogMatricula.show();
        }
    }

    public void dialogOnClick(View view) {
        SharedPref.write(FILIAL, (String) ((RadioButton) view).getText());
    }

    @Override
    public void onOkClicked(Matricula matricula) {
        homeViewModel.validaMatricula(matricula);
    }
}