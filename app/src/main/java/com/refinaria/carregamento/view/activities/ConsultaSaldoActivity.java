package com.refinaria.carregamento.view.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.refinaria.carregamento.R;
import com.refinaria.carregamento.databinding.ActivityConsultaSaldoBinding;
import com.refinaria.carregamento.model.Saldo;
import com.refinaria.carregamento.services.ScannerService;
import com.refinaria.carregamento.viewmodel.ConsultaSaldoViewModel;

import static com.refinaria.carregamento.utils.Utils.firstLetterUpperCase;

public class ConsultaSaldoActivity extends GenericActivity implements GenericActivity.IOnReceiveScan {

    ConsultaSaldoViewModel viewModel;
    ActivityConsultaSaldoBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_consulta_saldo);
        binding.setLifecycleOwner(this);

        binding.cardViewSaldo.setVisibility(View.GONE);

        setupViewModel();

        receiver = new ScanReceiver(this);
    }

    private void setupViewModel() {

        viewModel = ViewModelProviders.of(this).get(ConsultaSaldoViewModel.class);
        binding.setConsultaSaldoViewModel(viewModel);

        viewModel.getSaldo().observe(this, this::montaSaldo);
        viewModel.getProgressBar().observe(this, this::trataProgressBar);
        viewModel.getCardViewVisible().observe(this, this::cardViewVisible);
    }

    private void cardViewVisible(Boolean aBoolean) {
        if (aBoolean) binding.cardViewSaldo.setVisibility(View.VISIBLE);
        else binding.cardViewSaldo.setVisibility(View.INVISIBLE);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void montaSaldo(Saldo saldo) {
        binding.saldoDesc.setText(firstLetterUpperCase(saldo.getDsc2()));
        binding.saldoProduto.setText(saldo.getLitm());
        binding.saldoPallet.setText(saldo.getCnid());
        binding.saldoQuantidadeTotal.setText(saldo.getDsc1());
        binding.saldoQuantidadeDisponivel.setText(String.valueOf(saldo.getQnta()));
        binding.saldoLote.setText(saldo.getLotn());
    }

    private void trataProgressBar(Boolean aBoolean) {
        if (aBoolean) {
            binding.progressCircular.setIndeterminate(true);
            binding.layoutProgress.setVisibility(View.VISIBLE);
        } else binding.layoutProgress.setVisibility(View.INVISIBLE);
    }


    @Override
    public void received(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            String qrCode = bundle.getString(ScannerService.BAR_CODE);

            try {
                if (qrCode != null)
                    viewModel.recebeQrCode(qrCode);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}