package com.refinaria.carregamento.view.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.refinaria.carregamento.R;
import com.refinaria.carregamento.databinding.ActivitySelecionarClienteBinding;
import com.refinaria.carregamento.utils.Utils;
import com.refinaria.carregamento.view.adapters.ClientesAdapter;
import com.refinaria.carregamento.viewmodel.SelecionarClienteViewModel;

import java.util.ArrayList;
import java.util.Objects;

public class SelecionarClienteActivity extends AppCompatActivity {

    private ActivitySelecionarClienteBinding binding;
    private SelecionarClienteViewModel selecionarClienteViewModel;
    private ClientesAdapter clientesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_selecionar_cliente);
        binding.setLifecycleOwner(this);

        setupViewModel();
        setupListAdapter();
    }

    private void setupViewModel() {
        selecionarClienteViewModel = ViewModelProviders.of(this).get(SelecionarClienteViewModel.class);
        binding.setSelecionarClienteViewModel(selecionarClienteViewModel);

        selecionarClienteViewModel.getProgressBar().observe(this, this::trataProgressBar);
        selecionarClienteViewModel.getReturnMessage().observe(this, s -> Utils.longToast(this, s));
        selecionarClienteViewModel.getListaClientes().observe(this, this::trataClientes);
        selecionarClienteViewModel.getSucessoRepaletizacao().observe(this, this::sucessoRepaletizacao);

        selecionarClienteViewModel.init();
    }

    private void trataProgressBar(Boolean aBoolean) {
        if (aBoolean) {
            binding.progressCircular.setIndeterminate(true);
            binding.layoutProgress.setVisibility(View.VISIBLE);
        } else binding.layoutProgress.setVisibility(View.INVISIBLE);
    }

    private void trataClientes(ArrayList<com.refinaria.carregamento.model.clientes.Rowset> produtos) {
        clientesAdapter.updateList(produtos);
        clientesAdapter.notifyDataSetChanged();
    }

    private void setupListAdapter() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        binding.clienteRecyclerView.setLayoutManager(layoutManager);
        binding.clienteRecyclerView.addItemDecoration(new DividerItemDecoration(Objects.requireNonNull(this), DividerItemDecoration.VERTICAL));
        binding.clienteRecyclerView.setItemAnimator(new DefaultItemAnimator());

        clientesAdapter = new ClientesAdapter(new ArrayList<>(), this);
        binding.clienteRecyclerView.setAdapter(clientesAdapter);
    }

    private void sucessoRepaletizacao(Boolean sucesso) {
        if(sucesso) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            View layoutView = getLayoutInflater().inflate(R.layout.dialog_postive_layout, null);
            dialogBuilder.setView(layoutView);
            AlertDialog alertDialog = dialogBuilder.create();
            Objects.requireNonNull(alertDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            Objects.requireNonNull(alertDialog.getWindow()).getAttributes().windowAnimations = R.style.DialogAnimation;
            alertDialog.show();

            alertDialog.setOnDismissListener(dialogInterface -> {

                Intent intent = new Intent(this, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            });
        }
    }
}