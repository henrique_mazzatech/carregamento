package com.refinaria.carregamento.view.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.refinaria.carregamento.R;
import com.refinaria.carregamento.databinding.ActivityRepaletizacaoBinding;
import com.refinaria.carregamento.model.Rowset;
import com.refinaria.carregamento.services.ScannerService;
import com.refinaria.carregamento.utils.Utils;
import com.refinaria.carregamento.view.adapters.ProdutoAdapter;
import com.refinaria.carregamento.viewmodel.RepaletizacaoViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static com.refinaria.carregamento.utils.Constantes.TOAST_ADICIONE_UM_PRODUTO;

public class RepaletizacaoActivity extends GenericActivity implements GenericActivity.IOnReceiveScan {

    private ActivityRepaletizacaoBinding binding;
    private RepaletizacaoViewModel repaletizacaoViewModel;
    private ProdutoAdapter produtoAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_repaletizacao);
        binding.setLifecycleOwner(this);

        setupViewModel();
        setupListAdapter();

        receiver = new ScanReceiver(this);
    }

    private void setupViewModel() {
        repaletizacaoViewModel = ViewModelProviders.of(this).get(RepaletizacaoViewModel.class);
        binding.setRepaletizacaoViewModel(repaletizacaoViewModel);

        repaletizacaoViewModel.getProgressBar().observe(this, this::trataProgressBar);
        repaletizacaoViewModel.getReturnMessage().observe(this, s -> Utils.longToast(this, s));
        repaletizacaoViewModel.getMutableProdutoList().observe(this, this::trataProdutoList);
        repaletizacaoViewModel.getErrorMessage().observe(this, this::trataErro);
    }

    private void setupListAdapter() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        binding.produtoRecyclerView.setLayoutManager(layoutManager);
        binding.produtoRecyclerView.addItemDecoration(new DividerItemDecoration(Objects.requireNonNull(this), DividerItemDecoration.VERTICAL));

        produtoAdapter = new ProdutoAdapter(new ArrayList<>(), this, position -> repaletizacaoViewModel.onClickCancel(position));
        binding.produtoRecyclerView.setAdapter(produtoAdapter);
    }

    public void onClickSelecionarCliente(View view) {
        try {
            if (Objects.requireNonNull(repaletizacaoViewModel.getMutableProdutoList().getValue()).size() == 0) {
                Utils.longToast(this, TOAST_ADICIONE_UM_PRODUTO);
                return;
            }

            Intent intent = new Intent(this, SelecionarClienteActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } catch (Exception e) {
            Utils.longToast(this, e.getLocalizedMessage());
        }
    }

    private void trataProgressBar(Boolean aBoolean) {
        if (aBoolean) {
            binding.progressCircular.setIndeterminate(true);
            binding.layoutProgress.setVisibility(View.VISIBLE);
        } else binding.layoutProgress.setVisibility(View.INVISIBLE);
    }

    private void trataProdutoList(List<Rowset> produtos) {
        // Pra ordenar com o item mais recente no topo da lista
        List<Rowset> newArrayList = new ArrayList<>(produtos);
        Collections.reverse(newArrayList);

        produtoAdapter.updateList(newArrayList);
        produtoAdapter.notifyDataSetChanged();
    }

    private void trataErro(String erro) {
        if (erro.contains("palete")) {
            binding.editTextNumeroPalete.setError(erro);
            binding.editTextNumeroPalete.requestFocus();
        } else {
            binding.editTextNumeroPalete.setError(null);
        }
    }

    @Override
    public void received(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            String qrCode = bundle.getString(ScannerService.BAR_CODE);
            log(
                    qrCode
                            + bundle.getString(ScannerService.CODE_TYPE)
                            + bundle.getInt(ScannerService.LENGTH) + "\n");

            try {
                if (qrCode != null)
                    repaletizacaoViewModel.recebeQrCode(qrCode);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}