package com.refinaria.carregamento.model;

public class Saldo {
    private int qnta;
    private String cnid;
    private String litm;
    private String lotn;
    private String dsc1;
    private String dsc2;

    public String getCnid() {
        return cnid;
    }

    public void setCnid(String cnid) {
        this.cnid = cnid;
    }

    public String getLitm() {
        return litm;
    }

    public void setLitm(String litm) {
        this.litm = litm;
    }

    public String getLotn() {
        return lotn;
    }

    public void setLotn(String lotn) {
        this.lotn = lotn;
    }

    public String getDsc1() {
        return dsc1;
    }

    public void setDsc1(String dsc1) {
        this.dsc1 = dsc1;
    }

    public int getQnta() {
        return qnta;
    }

    public void setQnta(int qnta) {
        this.qnta = qnta;
    }

    public String getDsc2() { return dsc2; }

    public void setDsc2(String dsc2) { this.dsc2 = dsc2; }
}