
package com.refinaria.carregamento.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServiceRequest1 {

    @SerializedName("fs_P5542041_W5542041B")
    @Expose
    private FsP5542041W5542041B fsP5542041W5542041B;
    @SerializedName("stackId")
    @Expose
    private Integer stackId;
    @SerializedName("stateId")
    @Expose
    private Integer stateId;
    @SerializedName("rid")
    @Expose
    private String rid;
    @SerializedName("currentApp")
    @Expose
    private String currentApp;
    @SerializedName("timeStamp")
    @Expose
    private String timeStamp;
    @SerializedName("sysErrors")
    @Expose
    private List<Object> sysErrors = null;

    public FsP5542041W5542041B getFsP5542041W5542041B() {
        return fsP5542041W5542041B;
    }

    public void setFsP5542041W5542041B(FsP5542041W5542041B fsP5542041W5542041B) {
        this.fsP5542041W5542041B = fsP5542041W5542041B;
    }

    public Integer getStackId() {
        return stackId;
    }

    public void setStackId(Integer stackId) {
        this.stackId = stackId;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getCurrentApp() {
        return currentApp;
    }

    public void setCurrentApp(String currentApp) {
        this.currentApp = currentApp;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public List<Object> getSysErrors() {
        return sysErrors;
    }

    public void setSysErrors(List<Object> sysErrors) {
        this.sysErrors = sysErrors;
    }

}
