package com.refinaria.carregamento.model;

import com.refinaria.carregamento.utils.ColunasPickListEnum;

@SuppressWarnings("unused")
public class InputPickListModel {

    private ColunasPickListEnum name;
    private String value;

    public InputPickListModel() {
        super();
    }
    public ColunasPickListEnum getName() {
        return name;
    }

    public void setName(ColunasPickListEnum name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
