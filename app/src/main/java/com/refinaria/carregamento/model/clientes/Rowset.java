package com.refinaria.carregamento.model.clientes;

public class Rowset {
    private String qtdePallet;

    private String cliente;

    private String codCliente;

    public String getQtdePallet() {
        return qtdePallet;
    }

    public void setQtdePallet(String qtdePallet) {
        this.qtdePallet = qtdePallet;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(String codCliente) {
        this.codCliente = codCliente;
    }
}