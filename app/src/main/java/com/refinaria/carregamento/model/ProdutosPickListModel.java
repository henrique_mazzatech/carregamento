package com.refinaria.carregamento.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ProdutosPickListModel implements Cloneable, Parcelable {

    @SerializedName("ServiceRequest1")
    @Expose
    private ServiceRequest1 serviceRequest1;

    protected ProdutosPickListModel(Parcel in) {
    }

    public static final Creator<ProdutosPickListModel> CREATOR = new Creator<ProdutosPickListModel>() {
        @Override
        public ProdutosPickListModel createFromParcel(Parcel in) {
            return new ProdutosPickListModel(in);
        }

        @Override
        public ProdutosPickListModel[] newArray(int size) {
            return new ProdutosPickListModel[size];
        }
    };

    public ServiceRequest1 getServiceRequest1() {
        return serviceRequest1;
    }

    public void setServiceRequest1(ServiceRequest1 serviceRequest1) {
        this.serviceRequest1 = serviceRequest1;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
    }


}
