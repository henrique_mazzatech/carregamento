package com.refinaria.carregamento.model;

import java.util.List;

@SuppressWarnings("unused")
public class EstruturaPickListModel {

    private String username;
    private String password;
    private String usuarioLogin;
    private String matricula;
    private List<InputPickListModel> inputs;

    public EstruturaPickListModel() {

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsuarioLogin() {
        return usuarioLogin;
    }

    public void setUsuarioLogin(String usuarioLogin) {
        this.usuarioLogin = usuarioLogin;
    }

    public List<InputPickListModel> getInputs() {
        return inputs;
    }

    public void setInputs(List<InputPickListModel> inputs) {
        this.inputs = inputs;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public void montaHeader() {
        username = "IOT";
        password = "JAVA2018";
    }
}
