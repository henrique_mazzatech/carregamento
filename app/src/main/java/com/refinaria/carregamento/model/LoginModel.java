package com.refinaria.carregamento.model;

import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unused")
public class LoginModel {

    private static String usuario;
    private static String password;
    private static String auth;
    private static Map<String, String> header = new HashMap<>();
    private static Map<String, String> token = new HashMap<>();

    public LoginModel(String usuario, String password) {
        LoginModel.usuario = usuario;
        LoginModel.password = password;
    }

    public static String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        LoginModel.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        LoginModel.password = password;
    }

    public boolean isEmailValid() {
        return usuario != null && !usuario.isEmpty();
    }

    public boolean isPasswordValid() {
        return password != null && getPassword().length() > 5;
    }

    public String getAuth() {
        return auth;
    }

    public static void setAuth(String auth) {
        LoginModel.auth = auth;
    }

    public static Map<String, String> getToken() {
        return token;
    }

    public static void setToken(String token) {
        LoginModel.token.put("Authorization", token);
    }

    public static void setToken(Map<String, String> token) {
        LoginModel.token = token;
    }

    public static Map<String, String> getHeader() {
        header.put("Authorization", auth);
        header.put("Password", password);
        header.put("Username", usuario);

        return header;
    }
}
