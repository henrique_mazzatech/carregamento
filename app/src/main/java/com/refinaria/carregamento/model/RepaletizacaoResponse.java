package com.refinaria.carregamento.model;

import com.google.gson.annotations.SerializedName;

public class RepaletizacaoResponse {

    @SerializedName("doc")
    private int doc;

    @SerializedName("mensagem")
    private String mensagem;

    public int getDoc() {
        return doc;
    }

    public void setDoc(int doc) {
        this.doc = doc;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
}
