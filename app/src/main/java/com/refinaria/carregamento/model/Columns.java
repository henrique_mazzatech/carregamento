
package com.refinaria.carregamento.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Columns {

    @SerializedName("z_LITM_64")
    @Expose
    private String zLITM64;
    @SerializedName("z_CNID_59")
    @Expose
    private String zCNID59;
    @SerializedName("z_D200_60")
    @Expose
    private String zD20060;
    @SerializedName("z_DSC1_62")
    @Expose
    private String zDSC162;
    @SerializedName("z_EV01_77")
    @Expose
    private String zEV0177;
    @SerializedName("z_LNID_65")
    @Expose
    private String zLNID65;
    @SerializedName("z_LOTN_66")
    @Expose
    private String zLOTN66;
    @SerializedName("z_OTORDRLS_67")
    @Expose
    private String zOTORDRLS67;
    @SerializedName("z_DOCO_61")
    @Expose
    private String zDOCO61;
    @SerializedName("z_QNTA_68")
    @Expose
    private String zQNTA68;
    @SerializedName("z_SHPN_70")
    @Expose
    private String zSHPN70;
    @SerializedName("z_ITM_63")
    @Expose
    private String zITM63;
    @SerializedName("z_RLOT_69")
    @Expose
    private String zRLOT69;
    @SerializedName("z_UKID_71")
    @Expose
    private String zUKID71;

    public String getZLITM64() {
        return zLITM64;
    }

    public void setZLITM64(String zLITM64) {
        this.zLITM64 = zLITM64;
    }

    public String getZCNID59() {
        return zCNID59;
    }

    public void setZCNID59(String zCNID59) {
        this.zCNID59 = zCNID59;
    }

    public String getZD20060() {
        return zD20060;
    }

    public void setZD20060(String zD20060) {
        this.zD20060 = zD20060;
    }

    public String getZDSC162() {
        return zDSC162;
    }

    public void setZDSC162(String zDSC162) {
        this.zDSC162 = zDSC162;
    }

    public String getZEV0177() {
        return zEV0177;
    }

    public void setZEV0177(String zEV0177) {
        this.zEV0177 = zEV0177;
    }

    public String getZLNID65() {
        return zLNID65;
    }

    public void setZLNID65(String zLNID65) {
        this.zLNID65 = zLNID65;
    }

    public String getZLOTN66() {
        return zLOTN66;
    }

    public void setZLOTN66(String zLOTN66) {
        this.zLOTN66 = zLOTN66;
    }

    public String getZOTORDRLS67() {
        return zOTORDRLS67;
    }

    public void setZOTORDRLS67(String zOTORDRLS67) {
        this.zOTORDRLS67 = zOTORDRLS67;
    }

    public String getZDOCO61() {
        return zDOCO61;
    }

    public void setZDOCO61(String zDOCO61) {
        this.zDOCO61 = zDOCO61;
    }

    public String getZQNTA68() {
        return zQNTA68;
    }

    public void setZQNTA68(String zQNTA68) {
        this.zQNTA68 = zQNTA68;
    }

    public String getZSHPN70() {
        return zSHPN70;
    }

    public void setZSHPN70(String zSHPN70) {
        this.zSHPN70 = zSHPN70;
    }

    public String getZITM63() {
        return zITM63;
    }

    public void setZITM63(String zITM63) {
        this.zITM63 = zITM63;
    }

    public String getZRLOT69() {
        return zRLOT69;
    }

    public void setZRLOT69(String zRLOT69) {
        this.zRLOT69 = zRLOT69;
    }

    public String getZUKID71() {
        return zUKID71;
    }

    public void setZUKID71(String zUKID71) {
        this.zUKID71 = zUKID71;
    }

}
