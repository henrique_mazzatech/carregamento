package com.refinaria.carregamento.model.clientes;

public class JDE_SREQ_CONSULTA_PALETIZACAO_ESP_1
{
    private String formId;

    private Rowset[] rowset;

    private String records;

    private String moreRecords;

    private String gridId;

    private String title;

    public String getFormId ()
    {
        return formId;
    }

    public void setFormId (String formId)
    {
        this.formId = formId;
    }

    public Rowset[] getRowset ()
    {
        return rowset;
    }

    public void setRowset (Rowset[] rowset)
    {
        this.rowset = rowset;
    }

    public String getRecords ()
    {
        return records;
    }

    public void setRecords (String records)
    {
        this.records = records;
    }

    public String getMoreRecords ()
    {
        return moreRecords;
    }

    public void setMoreRecords (String moreRecords)
    {
        this.moreRecords = moreRecords;
    }

    public String getGridId ()
    {
        return gridId;
    }

    public void setGridId (String gridId)
    {
        this.gridId = gridId;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [formId = "+formId+", rowset = "+rowset+", records = "+records+", moreRecords = "+moreRecords+", gridId = "+gridId+", title = "+title+"]";
    }
}
