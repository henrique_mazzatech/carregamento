package com.refinaria.carregamento.services;

import android.content.Context;

public class ResourceProvider {

    private Context context;

    public ResourceProvider(Context mContext) {
        this.context = mContext;
    }

    public String getString(int resId) {
        return context.getString(resId);
    }
}