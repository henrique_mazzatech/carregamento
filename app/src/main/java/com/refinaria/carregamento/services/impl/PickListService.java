package com.refinaria.carregamento.services.impl;

import androidx.annotation.NonNull;

import com.refinaria.carregamento.model.EstruturaPickListModel;
import com.refinaria.carregamento.model.ProdutosPickListModel;
import com.refinaria.carregamento.model.RepaletizacaoResponse;
import com.refinaria.carregamento.model.clientes.ListaClientes;
import com.refinaria.carregamento.services.IPickListService;
import com.refinaria.carregamento.services.IServiceCallback;
import com.refinaria.carregamento.services.api.ApiModule;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressWarnings("unchecked")
public class PickListService implements IPickListService {
    @Override
    public void carregarPickList(Map<String, String> header, EstruturaPickListModel estruturaPickList, IServiceCallback callback) {
        ApiModule.getApi().carregarPick(estruturaPickList, header).enqueue(new Callback<ProdutosPickListModel>() {
            @Override
            public void onResponse(@NonNull Call<ProdutosPickListModel> call, @NonNull Response<ProdutosPickListModel> response) {
                callback.onSuccessResponse(call, response);
            }

            @Override
            public void onFailure(@NonNull Call<ProdutosPickListModel> call, @NonNull Throwable t) {
                callback.onErrorResponse(call, t);
            }
        });
    }

    @Override
    public void validaMatricula(String matricula, Map<String, String> header, IServiceCallback callback) {
        ApiModule.getApi().validaMatricula(matricula, header).enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(@NonNull Call<Boolean> call, @NonNull Response<Boolean> response) {
                callback.onSuccessResponse(call, response);
            }

            @Override
            public void onFailure(@NonNull Call<Boolean> call, @NonNull Throwable t) {
                callback.onErrorResponse(call, t);
            }
        });
    }

    @Override
    public void adicionarProdutoRepaletizacao(EstruturaPickListModel estruturaPickListModel, Map<String, String> header, IServiceCallback callback) {
        ApiModule.getApi().repaletizar(estruturaPickListModel, header).enqueue(new Callback<RepaletizacaoResponse>() {
            @Override
            public void onResponse(@NonNull Call<RepaletizacaoResponse> call, @NonNull Response<RepaletizacaoResponse> response) {
                callback.onSuccessResponse(call, response);
            }

            @Override
            public void onFailure(@NonNull Call<RepaletizacaoResponse> call, @NonNull Throwable t) {
                callback.onErrorResponse(call, t);
            }
        });
    }

    @Override
    public void listarClientes(EstruturaPickListModel estruturaPickListModel, Map<String, String> header, IServiceCallback callback) {
        ApiModule.getApi().listarClientes(estruturaPickListModel, header).enqueue(new Callback<ListaClientes>() {
            @Override
            public void onResponse(@NonNull Call<ListaClientes> call, @NonNull Response<ListaClientes> response) {
                callback.onSuccessResponse(call, response);
            }

            @Override
            public void onFailure(@NonNull Call<ListaClientes> call, @NonNull Throwable t) {
                callback.onErrorResponse(call, t);
            }
        });
    }
}