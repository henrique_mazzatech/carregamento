package com.refinaria.carregamento.services.api;

import com.google.gson.Gson;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiModule {

    private static String BASE_URL;

    private static Retrofit retrofit;
    private static IApiEndpoints api;
    private static Gson gson;

    public ApiModule(String BASE_URL){
        ApiModule.BASE_URL = BASE_URL;
    }

    private static Retrofit provideRetrofit() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return retrofit;
    }

    public static IApiEndpoints getApi() {
        if(api == null)
            api = ApiModule.provideRetrofit().create(IApiEndpoints.class);
        return api;
    }

    public static Gson getGsonInstance() {
        if(gson == null)
            gson = new Gson();

        return gson;
    }
}