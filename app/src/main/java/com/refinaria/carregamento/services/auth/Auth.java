package com.refinaria.carregamento.services.auth;

import android.util.Base64;

import java.nio.charset.Charset;

public abstract class Auth {
    public static byte[] generateToken(String key) {
        return Base64.encode(
                key.getBytes(Charset.forName("US-ASCII")),
                Base64.NO_WRAP | Base64.URL_SAFE
        );
    }

    public static String generateKey(String login, String password) {
        return login + ":" + password;
    }
}
