package com.refinaria.carregamento.services;

import java.util.Map;

public interface ILoginService {
    void login(Map<String, String> header, IServiceCallback callback);
}
