package com.refinaria.carregamento.services.impl;

import com.refinaria.carregamento.services.ILoginService;
import com.refinaria.carregamento.services.IServiceCallback;
import com.refinaria.carregamento.services.api.ApiModule;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressWarnings({"NullableProblems", "unchecked"})
public class LoginService implements ILoginService {

    @Override
    public void login(Map<String, String> header, final IServiceCallback callback) {
        ApiModule.getApi().login(header).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                callback.onSuccessResponse(call, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                callback.onErrorResponse(call, t);
            }
        });
    }
            }