package com.refinaria.carregamento.services.api;

import com.refinaria.carregamento.model.EstruturaPickListModel;
import com.refinaria.carregamento.model.ProdutosPickListModel;
import com.refinaria.carregamento.model.RepaletizacaoResponse;
import com.refinaria.carregamento.model.Saldo;
import com.refinaria.carregamento.model.clientes.ListaClientes;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface IApiEndpoints {
    @POST("/login")
    Call<ResponseBody> login(@HeaderMap Map<String, String> header);

    @POST("/rastreabilidade/carregarPick")
    Call<ProdutosPickListModel> carregarPick(@Body EstruturaPickListModel estruturaPickList, @HeaderMap Map<String, String> header);

    @POST("/rastreabilidade/carregarPick")
    Call<RepaletizacaoResponse> repaletizar(@Body EstruturaPickListModel estruturaPickList, @HeaderMap Map<String, String> header);

    @POST("/rastreabilidade/lista_clientes")
    Call<ListaClientes> listarClientes(@Body EstruturaPickListModel estruturaPickList, @HeaderMap Map<String, String> header);

    @GET("/rastreabilidade/matricula")
    Call<Boolean> validaMatricula(@Query("matricula") String matricula, @HeaderMap Map<String, String> header);

    @POST("/rastreabilidade/carregarPick")
    Call<Saldo> consultaSaldo(@Body EstruturaPickListModel estruturaPickList, @HeaderMap Map<String, String> header);
}