package com.refinaria.carregamento.services;

import com.refinaria.carregamento.model.EstruturaPickListModel;

public interface IConsultaSaldoService {
    void getSaldo(EstruturaPickListModel estruturaPickListModel);
}