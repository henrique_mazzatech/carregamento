package com.refinaria.carregamento.services;


import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.seuic.scanner.DecodeInfo;
import com.seuic.scanner.DecodeInfoCallBack;
import com.seuic.scanner.Scanner;
import com.seuic.scanner.ScannerFactory;
import com.seuic.scanner.ScannerKey;
import com.seuic.scanner.VideoCallBack;

@SuppressLint("Registered")
public class ScannerService extends Service implements DecodeInfoCallBack, VideoCallBack {
    static final String TAG = "ScannerApiTest";
    Scanner scanner;
    private boolean mScanRunning = false;

    private void log() {
        Log.i(TAG, "ScannerService");
    }

    Runnable runnable = new Runnable() {

        @Override
        public void run() {
            int ret1 = ScannerKey.open();
            if (ret1 > -1) {
                while (mScanRunning) {
                    log();
                    int ret = ScannerKey.getKeyEvent();
                    if (ret > -1) {
                        switch (ret) {
                            case ScannerKey.KEY_DOWN:
                                if (scanner != null && mScanRunning) {
                                    scanner.startScan();
                                }
                                break;
                            case ScannerKey.KEY_UP:
                                if (scanner != null && mScanRunning) {
                                    scanner.stopScan();
                                }
                                break;
                        }
                    }
                }
            }
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        scanner = ScannerFactory.getScanner(this);
        scanner.open();
        scanner.setDecodeInfoCallBack(this);
        scanner.setVideoCallBack(this);
        scanner.enable();
        mScanRunning = true;
        new Thread(runnable).start();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        mScanRunning = false;
        ScannerKey.close();
        scanner.setDecodeInfoCallBack(null);
        scanner.setVideoCallBack(null);
        scanner.close();
        scanner = null;
        super.onDestroy();
    }

    public static final String BAR_CODE = "scannerdata";
    public static final String CODE_TYPE = "codetype";
    public static final String LENGTH = "length";

    // this is a custom broadcast receiver action
    public static final String ACTION = "seuic.android.scanner.scannertestreciever";

    @Override
    public void onDecodeComplete(DecodeInfo info) {
        Intent intent = new Intent(ACTION);
        Bundle bundle = new Bundle();
        bundle.putString(BAR_CODE, info.barcode);
        bundle.putString(CODE_TYPE, info.codetype);
        bundle.putInt(LENGTH, info.length);
        intent.putExtras(bundle);
        sendBroadcast(intent);
    }

    @Override
    public boolean onVideoCallBack(int i, int i1, byte[] bytes) {
        return false;
    }
}