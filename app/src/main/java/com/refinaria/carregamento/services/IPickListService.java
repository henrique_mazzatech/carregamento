package com.refinaria.carregamento.services;

import com.refinaria.carregamento.model.EstruturaPickListModel;

import java.util.Map;

public interface IPickListService {
    void carregarPickList(Map<String, String> header, EstruturaPickListModel estruturaPickList, IServiceCallback callback);
    void validaMatricula(String matricula, Map<String, String> header, IServiceCallback callback);
    void adicionarProdutoRepaletizacao(EstruturaPickListModel estruturaPickListModel, Map<String, String> header, IServiceCallback callback);
    void listarClientes(EstruturaPickListModel estruturaPickListModel, Map<String, String> header, IServiceCallback callback);
}
