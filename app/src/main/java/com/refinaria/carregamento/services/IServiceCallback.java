package com.refinaria.carregamento.services;

import retrofit2.Call;
import retrofit2.Response;

public interface IServiceCallback<T> {
    void onSuccessResponse(Call<T> call, Response<T> response);
    void onErrorResponse(Call<T> call, Throwable t);
}
