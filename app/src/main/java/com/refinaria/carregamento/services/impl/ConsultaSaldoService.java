package com.refinaria.carregamento.services.impl;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.refinaria.carregamento.model.EstruturaPickListModel;
import com.refinaria.carregamento.model.LoginModel;
import com.refinaria.carregamento.model.Saldo;
import com.refinaria.carregamento.services.IConsultaSaldoService;
import com.refinaria.carregamento.services.api.ApiModule;
import com.refinaria.carregamento.viewmodel.ConsultaSaldoViewModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConsultaSaldoService implements IConsultaSaldoService {

    private ConsultaSaldoViewModel viewModel;

    public ConsultaSaldoService (ConsultaSaldoViewModel viewModel) {
        this.viewModel = viewModel;
    }

    @Override
    public void getSaldo(EstruturaPickListModel estruturaPickListModel) {
        Log.i("estruturaPickListModel", new Gson().toJson(estruturaPickListModel));
        ApiModule.getApi().consultaSaldo(estruturaPickListModel, LoginModel.getToken()).enqueue(new Callback<Saldo>() {
            @Override
            public void onResponse(@NonNull Call<Saldo> call, @NonNull Response<Saldo> response) {
                viewModel.getSaldoOnSuccess(response);
            }

            @Override
            public void onFailure(@NonNull Call<Saldo> call, @NonNull Throwable t) {
            }
        });
    }
}