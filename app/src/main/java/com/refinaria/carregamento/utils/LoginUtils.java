package com.refinaria.carregamento.utils;

import okhttp3.ResponseBody;
import retrofit2.Response;

public class LoginUtils {

    private static final int UNAUTHORIZED = 401;
    private static final int OK = 200;

    public static int trataRespostaLogin(Response<ResponseBody> response){
        switch (response.code()) {
            case UNAUTHORIZED:
                return 2;
            case OK:
                return 1;
            default:
                return 0;
        }
    }
}