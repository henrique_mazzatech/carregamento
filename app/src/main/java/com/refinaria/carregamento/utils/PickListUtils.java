package com.refinaria.carregamento.utils;

import com.refinaria.carregamento.model.EstruturaPickListModel;
import com.refinaria.carregamento.model.InputPickListModel;
import com.refinaria.carregamento.model.LoginModel;

import java.util.ArrayList;
import java.util.List;

public class PickListUtils {

    private static final String MODO_CANCELAMENTO = "EC";
    private static final String MODO_FRACIONADO = "EU";

    public static EstruturaPickListModel montaEstruturaPickList(String remessa, String numeroPedido, String numeroPalete) {
        EstruturaPickListModel estruturaPickList = new EstruturaPickListModel();
        estruturaPickList.montaHeader();
        estruturaPickList.setUsuarioLogin(LoginModel.getUsuario());
        estruturaPickList.setInputs(montaLista(remessa, numeroPedido, numeroPalete));

        return estruturaPickList;
    }

    public static EstruturaPickListModel montaEstruturaPickList(String remessa, String numeroPedido, int faturamento) {
        EstruturaPickListModel estruturaPickList = new EstruturaPickListModel();
        estruturaPickList.montaHeader();
        estruturaPickList.setUsuarioLogin(LoginModel.getUsuario());
        estruturaPickList.setInputs(montaLista(remessa, numeroPedido, faturamento));

        return estruturaPickList;
    }

    public static EstruturaPickListModel montaEstruturaCancelaProduto(String remessa, String numeroPedido, String numeroPalete, String quantidade) {
        EstruturaPickListModel estruturaPickList = new EstruturaPickListModel();
        estruturaPickList.montaHeader();
        estruturaPickList.setUsuarioLogin(LoginModel.getUsuario());
        estruturaPickList.setInputs(montaListaCancelaPallet(remessa, numeroPedido, numeroPalete, quantidade));

        return estruturaPickList;
    }

    public static EstruturaPickListModel montaEstruturaPickListFracionada(String remessa, String numeroPedido, String numeroPalete, String quantidade) {
        EstruturaPickListModel estruturaPickList = new EstruturaPickListModel();
        estruturaPickList.montaHeader();
        estruturaPickList.setUsuarioLogin(LoginModel.getUsuario());
        estruturaPickList.setInputs(montaLista(remessa, numeroPedido, numeroPalete, quantidade));

        return estruturaPickList;
    }

    public static EstruturaPickListModel montaEstruturaPickListConferir(String remessa, String numeroPedido) {
        EstruturaPickListModel estruturaPickList = new EstruturaPickListModel();
        estruturaPickList.montaHeader();
        estruturaPickList.setUsuarioLogin(LoginModel.getUsuario());
        estruturaPickList.setInputs(montaListaConferir(remessa, numeroPedido));

        return estruturaPickList;
    }

    private static List<InputPickListModel> montaListaConferir(String remessa, String numeroPedido) {
        List<InputPickListModel> inputPickLists = new ArrayList<>();
        InputPickListModel pickListComando = new InputPickListModel();
        InputPickListModel pickListShpn = new InputPickListModel();
        InputPickListModel pickListOtor = new InputPickListModel();

        pickListComando.setName(ColunasPickListEnum.COMANDO);
        pickListComando.setValue("EF");

        pickListShpn.setName((ColunasPickListEnum.SHPN));
        pickListShpn.setValue(remessa);

        pickListOtor.setName(ColunasPickListEnum.OTORDRLS);
        pickListOtor.setValue(numeroPedido);

        inputPickLists.add(pickListComando);
        inputPickLists.add(pickListOtor);
        inputPickLists.add(pickListShpn);

        return inputPickLists;
    }

    private static List<InputPickListModel> montaLista(String remessa, String numeroPedido, String numeroPalete) {
        List<InputPickListModel> inputPickLists = new ArrayList<>();
        InputPickListModel pickListComando = new InputPickListModel();
        InputPickListModel pickListShpn = new InputPickListModel();
        InputPickListModel pickListOtor = new InputPickListModel();
        InputPickListModel pickListCnid = new InputPickListModel();

        pickListComando.setName(ColunasPickListEnum.COMANDO);
        pickListComando.setValue("EP");

        pickListShpn.setName((ColunasPickListEnum.SHPN));
        pickListShpn.setValue(remessa);

        pickListOtor.setName(ColunasPickListEnum.OTORDRLS);
        pickListOtor.setValue(numeroPedido);

        pickListCnid.setName(ColunasPickListEnum.CNID);
        pickListCnid.setValue(numeroPalete);

        inputPickLists.add(pickListComando);
        inputPickLists.add(pickListOtor);
        inputPickLists.add(pickListCnid);
        inputPickLists.add(pickListShpn);

        return inputPickLists;
    }

    private static List<InputPickListModel> montaLista(String remessa, String numeroPedido, int faturamento) {
        List<InputPickListModel> inputPickLists = new ArrayList<>();
        InputPickListModel pickListComando = new InputPickListModel();
        InputPickListModel pickListShpn = new InputPickListModel();
        InputPickListModel pickListOtor = new InputPickListModel();
        InputPickListModel pickListFaturamento = new InputPickListModel();

        pickListComando.setName(ColunasPickListEnum.COMANDO);
        pickListComando.setValue("ER");

        pickListShpn.setName((ColunasPickListEnum.SHPN));
        pickListShpn.setValue(remessa);

        pickListOtor.setName(ColunasPickListEnum.OTORDRLS);
        pickListOtor.setValue(numeroPedido);

        pickListFaturamento.setName(ColunasPickListEnum.EV01);
        pickListFaturamento.setValue(String.valueOf(faturamento));

        inputPickLists.add(pickListComando);
        inputPickLists.add(pickListOtor);
        inputPickLists.add(pickListShpn);
        inputPickLists.add(pickListFaturamento);

        return inputPickLists;
    }

    private static List<InputPickListModel> montaListaCancelaPallet(String remessa, String numeroPedido, String numeroPalete, String quantidade) {
        List<InputPickListModel> inputPickLists = new ArrayList<>();
        InputPickListModel pickListComando = new InputPickListModel();
        InputPickListModel pickListShpn = new InputPickListModel();
        InputPickListModel pickListOtor = new InputPickListModel();
        InputPickListModel pickListCnid = new InputPickListModel();
        InputPickListModel pickListQtdd = new InputPickListModel();

        pickListComando.setName(ColunasPickListEnum.COMANDO);
        pickListComando.setValue(PickListUtils.MODO_CANCELAMENTO);

        pickListShpn.setName((ColunasPickListEnum.SHPN));
        pickListShpn.setValue(remessa);

        pickListOtor.setName(ColunasPickListEnum.OTORDRLS);
        pickListOtor.setValue(numeroPedido);

        pickListCnid.setName(ColunasPickListEnum.CNID);
        pickListCnid.setValue(numeroPalete);

        pickListQtdd.setName(ColunasPickListEnum.QNTA);
        pickListQtdd.setValue(quantidade);

        inputPickLists.add(pickListComando);
        inputPickLists.add(pickListOtor);
        inputPickLists.add(pickListCnid);
        inputPickLists.add(pickListShpn);
        inputPickLists.add(pickListQtdd);

        return inputPickLists;
    }

    private static List<InputPickListModel> montaLista(String remessa, String numeroPedido, String numeroPalete, String quantidade) {
        List<InputPickListModel> inputPickLists = new ArrayList<>();
        InputPickListModel pickListComando = new InputPickListModel();
        InputPickListModel pickListShpn = new InputPickListModel();
        InputPickListModel pickListOtor = new InputPickListModel();
        InputPickListModel pickListCnid = new InputPickListModel();
        InputPickListModel pickListQtdd = new InputPickListModel();

        pickListComando.setName(ColunasPickListEnum.COMANDO);
        pickListComando.setValue(PickListUtils.MODO_FRACIONADO);

        pickListShpn.setName((ColunasPickListEnum.SHPN));
        pickListShpn.setValue(remessa);

        pickListOtor.setName(ColunasPickListEnum.OTORDRLS);
        pickListOtor.setValue(numeroPedido);

        pickListCnid.setName(ColunasPickListEnum.CNID);
        pickListCnid.setValue(numeroPalete);

        pickListQtdd.setName(ColunasPickListEnum.QNTA);
        pickListQtdd.setValue(quantidade);

        inputPickLists.add(pickListComando);
        inputPickLists.add(pickListOtor);
        inputPickLists.add(pickListCnid);
        inputPickLists.add(pickListShpn);
        inputPickLists.add(pickListQtdd);

        return inputPickLists;
    }
}