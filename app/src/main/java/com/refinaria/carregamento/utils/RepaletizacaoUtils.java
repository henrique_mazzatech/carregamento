package com.refinaria.carregamento.utils;

import com.refinaria.carregamento.model.EstruturaPickListModel;
import com.refinaria.carregamento.model.InputPickListModel;
import com.refinaria.carregamento.model.LoginModel;
import com.refinaria.carregamento.model.Rowset;

import java.util.ArrayList;
import java.util.List;

public class RepaletizacaoUtils {

    public static Rowset montaRowsetViaQrCode(String qrCode) {
        Rowset produto = new Rowset();
        String[] entradasQrCode = qrCode.split(";");

        try {
            produto.setZCNID59(entradasQrCode[1]);
            produto.setZLOTN66(entradasQrCode[2]);
            produto.setZLITM64(entradasQrCode[4]);
            produto.setZQNTA68(Integer.valueOf(entradasQrCode[8]));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return produto;

    }

    public static EstruturaPickListModel estruturaAdicionarPallet(String filial, String value, String documentValue) {

        EstruturaPickListModel estruturaPickListModel = new EstruturaPickListModel();
        estruturaPickListModel.montaHeader();
        estruturaPickListModel.setUsuarioLogin(LoginModel.getUsuario());
        estruturaPickListModel.setInputs(montaInputsAdicionarPallet(filial, value, documentValue));


        return estruturaPickListModel;
    }

    private static List<InputPickListModel> montaInputsAdicionarPallet(String filial, String numeroPallet, String documentValue) {
        List<InputPickListModel> inputPickLists = new ArrayList<>();
        InputPickListModel pickListComando = new InputPickListModel();
        InputPickListModel pickListCnid = new InputPickListModel();
        InputPickListModel pickListDoc = new InputPickListModel();
        InputPickListModel pickListMcu = new InputPickListModel();

        pickListComando.setName(ColunasPickListEnum.COMANDO);
        pickListComando.setValue("EA");

        pickListCnid.setName((ColunasPickListEnum.CNID));
        pickListCnid.setValue(numeroPallet);

        pickListDoc.setName(ColunasPickListEnum.DOC);
        pickListDoc.setValue(documentValue);

        pickListMcu.setName(ColunasPickListEnum.MCU);
        pickListMcu.setValue(filial);

        inputPickLists.add(pickListComando);
        inputPickLists.add(pickListDoc);
        inputPickLists.add(pickListCnid);
        inputPickLists.add(pickListMcu);

        return inputPickLists;

    }

    public static EstruturaPickListModel estruturaRemovePallet(String codigoFilial, String numeroPallet, String documentValue) {
        EstruturaPickListModel estruturaPickListModel = new EstruturaPickListModel();
        estruturaPickListModel.montaHeader();
        estruturaPickListModel.setUsuarioLogin(LoginModel.getUsuario());
        estruturaPickListModel.setInputs(montaInputsRemovePallet(codigoFilial, numeroPallet, documentValue));

        return estruturaPickListModel;
    }

    private static List<InputPickListModel> montaInputsRemovePallet(String codigoFilial, String numeroPallet, String documentValue) {
        List<InputPickListModel> inputPickLists = new ArrayList<>();
        InputPickListModel pickListComando = new InputPickListModel();
        InputPickListModel pickListCnid = new InputPickListModel();
        InputPickListModel pickListDoc = new InputPickListModel();
        InputPickListModel pickListMcu = new InputPickListModel();

        pickListComando.setName(ColunasPickListEnum.COMANDO);
        pickListComando.setValue("ES");

        pickListDoc.setName(ColunasPickListEnum.DOC);
        pickListDoc.setValue(documentValue);

        pickListCnid.setName(ColunasPickListEnum.CNID);
        pickListCnid.setValue(numeroPallet);

        pickListMcu.setName(ColunasPickListEnum.MCU);
        pickListMcu.setValue(codigoFilial);

        inputPickLists.add(pickListComando);
        inputPickLists.add(pickListDoc);
        inputPickLists.add(pickListCnid);
        inputPickLists.add(pickListMcu);

        return inputPickLists;
    }

    public static EstruturaPickListModel estruturaListaClientes(String numeroItem) {
        EstruturaPickListModel estruturaPickListModel = new EstruturaPickListModel();
        estruturaPickListModel.montaHeader();
        estruturaPickListModel.setUsuarioLogin(LoginModel.getUsuario());
        estruturaPickListModel.setInputs(montaInputsListaClientes(numeroItem));

        return estruturaPickListModel;
    }

    private static List<InputPickListModel> montaInputsListaClientes(String numeroItem) {
        List<InputPickListModel> inputPickLists = new ArrayList<>();
        InputPickListModel pickListItem = new InputPickListModel();

        pickListItem.setName(ColunasPickListEnum.litm);
        pickListItem.setValue(numeroItem);

        inputPickLists.add(pickListItem);

        return inputPickLists;
    }

    public static EstruturaPickListModel estruturaFinalizaRepaletizacao(String numeroItem, String doc, String codigoCliente) {
        EstruturaPickListModel estruturaPickListModel = new EstruturaPickListModel();
        estruturaPickListModel.montaHeader();
        estruturaPickListModel.setUsuarioLogin(LoginModel.getUsuario());
        estruturaPickListModel.setInputs(montaInputsFinalizaRepaletizacao(numeroItem, doc, codigoCliente));

        return estruturaPickListModel;
    }

    private static List<InputPickListModel> montaInputsFinalizaRepaletizacao(String numeroItem, String doc, String codigoCliente) {
        List<InputPickListModel> inputPickLists = new ArrayList<>();
        InputPickListModel pickListComando = new InputPickListModel();
        InputPickListModel pickListItem = new InputPickListModel();
        InputPickListModel pickListDoc = new InputPickListModel();
        InputPickListModel pickListAn8 = new InputPickListModel();

        pickListComando.setName(ColunasPickListEnum.COMANDO);
        pickListComando.setValue("EL");

        pickListItem.setName(ColunasPickListEnum.litm);
        pickListItem.setValue(numeroItem);

        pickListDoc.setName(ColunasPickListEnum.DOC);
        pickListDoc.setValue(doc);

        pickListAn8.setName(ColunasPickListEnum.AN8);
        pickListAn8.setValue(codigoCliente);

        inputPickLists.add(pickListComando);
        inputPickLists.add(pickListItem);
        inputPickLists.add(pickListDoc);
        inputPickLists.add(pickListAn8);

        return inputPickLists;
    }
}