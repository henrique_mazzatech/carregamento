package com.refinaria.carregamento.utils;

import androidx.annotation.NonNull;

@SuppressWarnings("unused")
public enum ColunasPickListEnum {
    //Item
    COMANDO("Comando"),
    SHPN("Numero do Embarque"),
    OTORDRLS("Order Release"),
    CNID("Número do Pallet"),
    QNTA("Quantidade fracionada"),
    LNID("Número da Linha JDE"),
    EV01("Tipo de faturamento"),
    DOC("Numero do documento"),
    MCU("Código da filial"),
    litm("Item selecionado"),
    AN8("código do cliente "),

    //Estrutura json
    NAME_DETAIL_INPUTS("GridDataModel");

    private final String descricao;

    ColunasPickListEnum(String descricao){
        this.descricao = descricao;
    }

    @NonNull
    @Override
    public String toString(){
        return getDescricao();
    }

    public String getDescricao() {
        return descricao;
    }
}