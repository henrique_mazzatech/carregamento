package com.refinaria.carregamento.utils;

public class Constantes {
    public static boolean DEBUG = true;

    public static String FLAG_USUARIO = "USUARIO";
    public static String FLAG_BARCODE = "BARCODE_VALUE";
    public static String FLAG_QR_CODE = "QR_CODE_VALUE";
    public static String FLAG_AUTHORIZATION = "Authorization";
    public static String FLAG_REMESSA = "REMESSA";
    public static String FLAG_PEDIDO = "PEDIDO";
    public static String FLAG_FATURAMENTO = "FATURAMENTO";

    public static String ERRO_TAMANHO_REMESSA_INVALIDO = "A remessa deve conter pelo menos 6 caracteres";
    public static String ERRO_TAMANHO_NUMERO_PALLET_INVALIDO = "O código do palete deve conter pelo menos 5 caracteres";
    public static String ERRO_NUMERO_PEDIDO_INVALIDO = "Insira um pedido válido!\nEx: 1111+AA+11111-1";

    public static String ERRO_DIGITE_UM_USUARIO = "Digite um usuário";
    public static String ERRO_USUARIO_INVALIDO = "Digite um usuário válido";
    public static String ERRO_SENHA_INVALIDA = "A senha deve conter pelo menos 6 caracteres";
    public static String ERRO_PRODUTO_REMOVIDO = "Erro ao remover o pallet!";

    public static String TOAST_PRODUTO_ADICIONADO = "Produto adicionado com sucesso!";
    public static String TOAST_PRODUTO_REMOVIDO = "Produto removido com sucesso!";
    public static String TOAST_SELECIONE_UM_CLIENTE = "Selecione um cliente";
    public static String TOAST_ADICIONE_UM_PRODUTO = "Adicione algum produto";

    public static String MATRICULA_INVALIDA = "Matrícula inválida";

    public static String USUARIO_WSR = "henrique.mazzatech";
    public static String SENHA_WSR = "123mudar";
    //    public static String USUARIO_WSR = "iot.rastreabilidade";
    //    public static String SENHA_WSR = "JAVA2019";

    public static String FILIAL = "FILIAL";
    public static String CODIGO_CABO_FRIO = "1410";
    public static String CODIGO_CABO_FRIO_MENSALISTA = "1400";
    public static String CODIGO_GUARULHOS = "2400";
    public static String CABO_FRIO = "Cabo Frio";
    public static String GUARULHOS = "Guarulhos";

    public static String SHARED_QR_CODE   = "QR_CODE";
    public static String SHARED_COD_PROD  = "COD_PROD";
    public static String SHARED_DOCUMENTO = "DOCUMENTO";
    public static String SHARED_COD_CLIENTE = "QTD_CLIENTE";

}
