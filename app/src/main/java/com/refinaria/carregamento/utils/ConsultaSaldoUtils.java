package com.refinaria.carregamento.utils;

import com.refinaria.carregamento.model.EstruturaPickListModel;
import com.refinaria.carregamento.model.InputPickListModel;
import com.refinaria.carregamento.model.LoginModel;

import java.util.ArrayList;
import java.util.List;

public class ConsultaSaldoUtils {

    public static EstruturaPickListModel montaEstruturaConsultaSaldo(String numeroPallet) {
        EstruturaPickListModel estruturaPickList = new EstruturaPickListModel();
        estruturaPickList.montaHeader();
        estruturaPickList.setUsuarioLogin(LoginModel.getUsuario());
        estruturaPickList.setInputs(montaLista(numeroPallet));

        return estruturaPickList;
    }

    private static List<InputPickListModel> montaLista(String numeroPallet) {
        List<InputPickListModel> inputPickLists = new ArrayList<>();
        InputPickListModel pickListComando = new InputPickListModel();
        InputPickListModel pickListCnid = new InputPickListModel();

        pickListComando.setName(ColunasPickListEnum.COMANDO);
        pickListComando.setValue(ComandosJDE.EB.getDescricao());

        pickListCnid.setName(ColunasPickListEnum.CNID);
        pickListCnid.setValue(numeroPallet);

        inputPickLists.add(pickListComando);
        inputPickLists.add(pickListCnid);

        return inputPickLists;
    }
}