package com.refinaria.carregamento.utils;

public class Matricula {

    private String prefixo;
    private String sufixo;

    public Matricula(String prefixo, String sufixo) {
        this.prefixo = prefixo;
        this.sufixo = sufixo;
    }

    public String getMatricula() {
        return prefixo + sufixo;
    }

    public String getPrefixo() {
        return prefixo;
    }

    public String getSufixo() {
        return sufixo;
    }

    public void setPrefixo(String prefixo) {
        this.prefixo = prefixo;
    }

    public void setSufixo(String sufixo) {
        this.sufixo = sufixo;
    }
}