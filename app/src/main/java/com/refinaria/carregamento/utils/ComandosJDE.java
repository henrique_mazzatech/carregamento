package com.refinaria.carregamento.utils;

import androidx.annotation.NonNull;

public enum ComandosJDE {
    EB("EB"); // Consulta Saldo

    private String descricao;

    ComandosJDE(String descricao) {
        this.descricao = descricao;
    }

    @NonNull
    @Override
    public String toString(){
        return getDescricao();
    }

    public String getDescricao() {
        return descricao;
    }
}
