package com.refinaria.carregamento.utils;

import android.content.Context;
import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.refinaria.carregamento.R;

abstract class AbstractDialog {

    private final static String OK = "OK";
    private final static String CANCEL = "CANCEL";

    public static DialogFragment dialogFragment;

    static void criaDialogo(String title, String corpo, Context context) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(corpo);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(OK, (dialog, which) -> {
        });
        alertDialogBuilder.show();
    }

    static void criaDialogo(String corpo, Context context) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context, R.style.dialogSuccess);
        alertDialogBuilder.setMessage(corpo);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(OK, (dialog, which) -> {
        });
        alertDialogBuilder.show();
    }

    static void criaDialogo(String title, String corpo, Context context, DialogInterface.OnClickListener positiveListener, DialogInterface.OnClickListener negativeListener) {
        if (dialogFragment != null)
            dialogFragment.dismiss();
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title)
                .setMessage(corpo)
                .setPositiveButton(OK, positiveListener)
                .setNegativeButton(CANCEL, negativeListener)
                .show();
    }
}
