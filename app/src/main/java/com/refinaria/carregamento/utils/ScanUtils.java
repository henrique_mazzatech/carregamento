package com.refinaria.carregamento.utils;

import java.util.Objects;

public class ScanUtils {
    public static String getRemessa(String barcode) {
        return barcode.contains("/") ? barcode.split("/")[0] : "";
    }

    public static String getNumeroPedido(String barcode) {
        return barcode.contains("/") ? barcode.split("/")[1] : "";
    }

    public static String getNumeroPalete(String qrCode) {
        return qrCode.contains(";") ? qrCode.split(";")[1] : "";
    }

    public static String getCodigoProduto(String qrCode) {
        return qrCode.contains(";") ? qrCode.split(";")[4] : "";
    }

    public static String validaNumeroPallet(String value) {
        if (Objects.equals(value, "") || DataUtils.isCampoValido(value, 5))
            return "";
        else
            return (Constantes.ERRO_TAMANHO_NUMERO_PALLET_INVALIDO);
    }
}