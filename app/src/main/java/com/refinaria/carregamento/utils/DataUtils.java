package com.refinaria.carregamento.utils;

@SuppressWarnings("WeakerAccess")
public class DataUtils {

    public static boolean greaterThan(String string, int size) {
        return string.length() > size;
    }

    public static boolean isCampoValido(String string, int size) {
        return string != null && greaterThan(string, size);
    }
}
