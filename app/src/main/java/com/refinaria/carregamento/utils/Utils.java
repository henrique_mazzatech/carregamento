package com.refinaria.carregamento.utils;

import android.app.Activity;
import android.os.Build;
import android.view.Gravity;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.refinaria.carregamento.model.Rowset;
import com.refinaria.carregamento.services.SharedPref;

import java.util.ArrayList;
import java.util.List;

import static com.refinaria.carregamento.utils.Constantes.CABO_FRIO;
import static com.refinaria.carregamento.utils.Constantes.CODIGO_CABO_FRIO;
import static com.refinaria.carregamento.utils.Constantes.CODIGO_GUARULHOS;
import static com.refinaria.carregamento.utils.Constantes.FILIAL;

public class Utils {

    public static String firstUppercase(String str) {
        char[] ch = str.toCharArray();
        for (int i = 0; i < str.length(); i++) {
            if (i == 0 && ch[i] != ' ' ||
                    ch[i] != ' ' && ch[i - 1] == ' ') {
                if (ch[i] >= 'a' && ch[i] <= 'z') {
                    ch[i] = (char) (ch[i] - 'a' + 'A');
                }
            } else if (ch[i] >= 'A' && ch[i] <= 'Z')
                ch[i] = (char) (ch[i] + 'a' - 'A');
        }
        return new String(ch);
    }

    private static void toastMessage(Activity activity, String s, int lengh) {
        Toast toast = Toast.makeText(activity, s, lengh);
        toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 100);
        toast.show();
    }

    public static void longToast(Activity activity, String s) {
        toastMessage(activity, s, Toast.LENGTH_LONG);
    }

    public static void shortToast(Activity activity, String s) {
        toastMessage(activity, s, Toast.LENGTH_SHORT);
    }

    public static List<Rowset> filtraRowsetArray(List<Rowset> listaOriginal) {
        ArrayList<Rowset> arrayFiltrada = new ArrayList<>();
        for (Rowset row : listaOriginal)
            if (!row.getZCNID59().equals(" ") && !row.getZCNID59().isEmpty())
                arrayFiltrada.add(row);

        return arrayFiltrada;
    }

    public static String getPrefixoMatricula() {
        if (SharedPref.read(FILIAL, CABO_FRIO).equals(CABO_FRIO)) {
            return CODIGO_CABO_FRIO;
        } else {
            return CODIGO_GUARULHOS;
        }
    }

    public static String getCodigoFilial() {
        return Utils.getPrefixoMatricula().equals("2400") ? "13000" : "14000";
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static String firstLetterUpperCase(String sentence) {
        // split into words
        String[] words = sentence.split(" ");
        StringBuilder result = new StringBuilder();


        for (int i = 0; i < words.length; i++) {
            words[i] = words[i].substring(0, 1).toUpperCase() + words[i].substring(1).toLowerCase();
        }

        for (String word : words) {
            if (!word.equals(words[0])) result.append(" ");
            result.append(word);
        }

        return result.toString();
    }
}