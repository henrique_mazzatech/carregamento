package com.refinaria.carregamento.utils;

import android.content.Context;
import android.content.DialogInterface;

@SuppressWarnings("FieldCanBeLocal")
public class GeneralDialogFragment extends AbstractDialog {

    private final static String ERRO_INESPERADO = "Erro inesperado";
    private final static String ERRO_NAO_AUTORIZADO = "Não autorizado";
    private final static String ERRO = "Erro";

    private final static String EXCLUIR = "Excluir";
    private final static String CORPO_EXCLUIR = "Deseja realmente excluir o pedido?";

    private static final String OK = "OK";

    public static void dialogoErroInesperado(Context context) {
        criaDialogo(ERRO, ERRO_INESPERADO, context);
    }

    public static void dialogoNaoAutorizado(Context context) {
        criaDialogo(ERRO, ERRO_NAO_AUTORIZADO, context);
    }

    public static void dialogoExcluirPedido(Context context, DialogInterface.OnClickListener positiveListener, DialogInterface.OnClickListener negativeListener) {
        criaDialogo(EXCLUIR, CORPO_EXCLUIR, context, positiveListener, negativeListener);
    }

    public static void dialogoFalhaConferir(Context context, String corpo) {
        criaDialogo("", corpo, context);
    }

    public static void dialogoSucesso(Context context, String corpo) {
        criaDialogo(corpo, context);
    }
}