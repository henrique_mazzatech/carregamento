package com.refinaria.carregamento;

import com.refinaria.carregamento.model.LoginModel;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class LoginTestes {

    private String EMAIL_VALIDO = "teste@dominio.com";
    private String SENHA_VALIDA = "password";
    private String STRING_VAZIA = "";

    private LoginModel loginModel = new LoginModel(EMAIL_VALIDO, SENHA_VALIDA);

    @Test
    public void emailCorreto() {
        loginModel.setUsuario(EMAIL_VALIDO);
        assertTrue(loginModel.isEmailValid());
    }

    @Test
    public void emailVazio() {
        loginModel.setUsuario(STRING_VAZIA);
        assertFalse(loginModel.isEmailValid());
    }

    @Test
    public void emailNulo() {
        loginModel.setUsuario(null);
        assertFalse(loginModel.isEmailValid());
    }

    @Test
    public void senhaValida() {
        loginModel.setPassword(SENHA_VALIDA);
        assertTrue(loginModel.isPasswordValid());
    }

    @Test
    public void senhaInvalida() {
        loginModel.setPassword("senha");
        assertFalse(loginModel.isPasswordValid());
    }

    @Test
    public void senhaVazia() {
        loginModel.setPassword(STRING_VAZIA);
        assertFalse(loginModel.isPasswordValid());
    }

    @Test
    public void senhaSenhaNula() {
        loginModel.setPassword(null);
        assertFalse(loginModel.isPasswordValid());
    }
}