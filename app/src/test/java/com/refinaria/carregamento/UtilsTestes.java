package com.refinaria.carregamento;

import com.refinaria.carregamento.utils.Utils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UtilsTestes {

    @Test
    public void verificaFormatacaoFrase() {
        String fraseCaps = "SALINAS DA LAGOA GRANULADO";
        String fraseCerta = "Salinas Da Lagoa Granulado";
        assertEquals(fraseCerta, Utils.firstLetterUpperCase(fraseCaps));
    }
}